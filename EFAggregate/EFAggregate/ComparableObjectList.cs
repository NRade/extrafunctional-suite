﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate
{
    public class ComparableObjectList : List<object>
    {
        public override string ToString()
        {
            return String.Join("#", this.Where(o => o != null).Select(o => o.ToString()));
        }

        public override bool Equals(object obj)
        {
            return this.ToString() == obj.ToString();
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}
