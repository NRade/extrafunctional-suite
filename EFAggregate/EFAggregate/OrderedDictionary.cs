﻿using System.Collections.Generic;
using System.Linq;

namespace EFAggregate
{
	class OrderedDictionary<T1, T2> where T2 : new()
	{
		private List<T1> OrderedKeys = new List<T1>();

		private Dictionary<T1, T2> Dictionary = new Dictionary<T1, T2>();

		public IEnumerable<KeyValuePair<T1, T2>> OrderedCollection
		{
			get
			{
				return OrderedKeys.Select(key => new KeyValuePair<T1, T2>(key, this[key]));
			}
		}

		public T2 this[T1 key]
		{
			get
			{
				if (!Dictionary.TryGetValue(key, out T2 outputValue))
				{
					OrderedKeys.Add(key);
					outputValue = new T2();
					Dictionary.Add(key, outputValue);
				}
				return outputValue;
			}
		}
	}
}
