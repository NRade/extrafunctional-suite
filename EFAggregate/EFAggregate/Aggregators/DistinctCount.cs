﻿using EFAggregate.Aggregators.Support;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator("Count distinct")]
    public class DistinctCount<T1> : Aggregator<T1, int>
    {
        public override string GenerateDescription(string inputColumnName, string description)
        {
            return "Distinct count of: " + inputColumnName + (!String.IsNullOrEmpty(description) ? " - " : "") + description;
        }

        public override int GetAggregatedValue(List<T1> valueList)
        {
            return valueList.Distinct().Count(v => v != null);
        }

        public DistinctCount() : base()
        {
            dataProperties.length = 0;
            dataProperties.codepage = 0;
            dataProperties.precision = 0;
            dataProperties.scale = 0;
            dataProperties.type = DataType.DT_I4;
        }
    }
}