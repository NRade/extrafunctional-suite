﻿using EFAggregate.Aggregators.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator(GroupByName)]
    public class GroupBy : Aggregator
    {
        public const string GroupByName = "Group by";

        public override object GetAggregatedObject(ComparableObjectList groupByValues, Dictionary<int, List<object>> aggregateValues, int index)
        {
            return null;
        }
    }
}
