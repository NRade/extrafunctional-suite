﻿using EFAggregate.Aggregators.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator("Minimum")]
    public class Minimum<T1> : Aggregator<T1, T1>
    {
        protected override List<Type> GetIncludedTypes()
        {
            return new List<Type> { typeof(byte), typeof(short), typeof(int), typeof(long), typeof(DateTime), typeof(DateTimeOffset), typeof(double) };
        }

        public override string GenerateDescription(string inputColumnName, string description)
        {
            return "Minimum of: " + inputColumnName + (!String.IsNullOrEmpty(description) ? " - " : "") + description;
        }

        public override T1 GetAggregatedValue(List<T1> valueList)
        {
            if (!valueList.Any())
            {
                return default(T1);
            }

            return valueList.Min();
        }
    }
}