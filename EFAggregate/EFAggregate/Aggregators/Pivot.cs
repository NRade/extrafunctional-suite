﻿using EFAggregate.Aggregators.Support;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator("Pivot")]
    [HasArguments("inputColumn = 'value'")]
    public class Pivot : ComplexAggregator
    {
        private string pivotColumnName;
        private int pivotColumnIndex;
        private string value;

        public override void SetArguments(string arguments)
        {
            var splitArguments = arguments.Split('=');

            var splitValue = splitArguments[1].Split('\'');

            pivotColumnName = splitArguments[0].Trim();

            for(int i = 0; i < InputColumns.Count; i++)
            {
                if(InputColumns[i].Name == pivotColumnName)
                {
                    pivotColumnIndex = i;
                    break;
                }
            }

            value = splitValue[1];
        }

        public override string GenerateDescription(string inputColumnName, string description)
        {
            return "Value of column: " + inputColumnName + ", pivoted on " + pivotColumnName + " = '" + value + "'";
        }

        public override bool ArgumentsValid(string arguments, out string message)
        {
            if(String.IsNullOrEmpty(arguments))
            {
                message = "One input column should be equated to one value using the = symbol.";
                return false;
            }

            var splitArguments = arguments.Split('=');

            if(splitArguments.Length != 2)
            {
                message = "One input column should be equated to one value using the = symbol.";
                return false;
            } else
            {
                var splitValue = splitArguments[1].Split('\'');
                if(splitValue.Length != 3)
                {
                    message = "The value should be surrounded by \' symbols.";
                    return false;
                }

                if(!InputColumns.Cast<IDTSInputColumn100>().Any(i => i.Name.Trim() == splitArguments[0].Trim()))
                {
                    message = "The input column name was not recognized.";
                    return false;
                }
            }

            message = "";
            return true;
        }

        public override object GetAggregatedValue(ComparableObjectList groupByValues, Dictionary<int, List<object>> aggregateValues, int index)
        {
            var currentIndexValues = aggregateValues[index];
            for(int i = 0; i < currentIndexValues.Count; i++)
            {
                var pivotColumnValue = Convert.ToString(aggregateValues[pivotColumnIndex][i]);

                if (pivotColumnValue == value)
                {
                    return currentIndexValues[i];
                }
            }
            return null;
        }
    }
}
