﻿using EFAggregate.Aggregators.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator("Select first")]
    public class First<T1> : Aggregator<T1, T1>
    {
        public override string GenerateDescription(string inputColumnName, string description)
        {
            return "First non-null element of: " + inputColumnName + (!String.IsNullOrEmpty(description) ? " - " : "") + description;
        }

        public override T1 GetAggregatedValue(List<T1> valueList)
        {
            return valueList.FirstOrDefault(v => v != null);
        }
    }
}
