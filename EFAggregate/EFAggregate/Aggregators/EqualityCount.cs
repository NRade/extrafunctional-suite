﻿using EFAggregate.Aggregators.Support;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator("Equality Count")]
    public class EqualityCount<T1> : Aggregator<T1, string>
    {
        public override string GenerateDescription(string inputColumnName, string description)
        {
            return "Equality count of: " + inputColumnName + (!String.IsNullOrEmpty(description) ? " - " : "") + description;
        }

        public override string GetAggregatedValue(List<T1> valueList)
        {
            var maxEqualityCount = valueList.GroupBy(v => v).Max(g => g.Count());

            string symbolPrefix;
            if(maxEqualityCount == valueList.Count)
            {
                symbolPrefix = "[==] ";
            } else
            {
                symbolPrefix = "[!=] ";
            }

            return symbolPrefix + maxEqualityCount + "/" + valueList.Count;
        }

        public EqualityCount() : base()
        {
            dataProperties.length = 20;
            dataProperties.codepage = 0;
            dataProperties.precision = 0;
            dataProperties.scale = 0;
            dataProperties.type = DataType.DT_WSTR;
        }
    }
}
