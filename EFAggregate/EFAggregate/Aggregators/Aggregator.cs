﻿using EFAggregate.Aggregators.Support;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EFAggregate.Aggregators
{
	public abstract class Aggregator
	{
		protected DataProperties dataProperties = new DataProperties();

		protected string arguments;

		public virtual void SetArguments(string arguments)
		{
			this.arguments = arguments;
		}

		public virtual List<Type> GetInputDataTypes()
		{
			return null;
		}

		public virtual string GenerateDescription(string inputColumnName, string description)
		{
			return description;
		}

		public string GetDefaultArguments()
		{
			return GetType().GetCustomAttributes(typeof(HasArguments)).Select(ca => ((HasArguments)ca).DefaultArguments).FirstOrDefault();
		}

		private static IEnumerable<Type> AggregatorTypes
		{
			get
			{
				return Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace == "EFAggregate.Aggregators" && t.CustomAttributes.Any(ca => ca.AttributeType == typeof(IsAggregator)));
			}
		}

		public override string ToString()
		{
			return GetType().GetCustomAttributes().Where(a => a is IsAggregator).Select(a => ((IsAggregator)a).Name).FirstOrDefault();
		}

		public static Aggregator FromString(string name, Type genericType)
		{
			var type = AggregatorTypes.First(t => ((IsAggregator)t.GetCustomAttributes().First(a => a is IsAggregator)).Name == name);

			if (type.ContainsGenericParameters && genericType != null)
			{
				type = type.MakeGenericType(genericType);
			}

			return (Aggregator)Activator.CreateInstance(type);
		}

		public static string[] GetAggregatorNames()
		{
			return AggregatorTypes.Select(t => ((IsAggregator)t.GetCustomAttributes().First(ca => ca is IsAggregator)).Name).ToArray();
		}

		public abstract object GetAggregatedObject(ComparableObjectList groupByValues, Dictionary<int, List<object>> aggregateValues, int index);

		public void SetDataType(IDTSOutputColumn100 outputColumn, IDTSInputColumn100 inputColumn)
		{
			outputColumn.SetDataTypeProperties(
				dataProperties.type ?? inputColumn.DataType,
				dataProperties.length ?? inputColumn.Length,
				dataProperties.precision ?? inputColumn.Precision,
				dataProperties.scale ?? inputColumn.Scale,
				dataProperties.codepage ?? inputColumn.CodePage
				);
		}

		public virtual bool ArgumentsValid(string arguments, out string message)
		{
			message = null;
			return true;
		}

		protected virtual List<Type> GetIncludedTypes()
		{
			return null;
		}

		protected virtual List<Type> GetExcludedTypes()
		{
			return null;
		}

		public bool AcceptsDataType(Type type)
		{
			var includedTypes = GetIncludedTypes();
			var excludedTypes = GetExcludedTypes();

			if (includedTypes != null)
			{
				return includedTypes.Contains(type);
			}
			return excludedTypes == null || !excludedTypes.Contains(type);
		}
	}

	public abstract class Aggregator<TInput, TOutput> : Aggregator
	{
		protected override List<Type> GetIncludedTypes()
		{
			return new List<Type> { typeof(TInput) };
		}

		public abstract TOutput GetAggregatedValue(List<TInput> valueList);

		public override object GetAggregatedObject(ComparableObjectList groupByValues, Dictionary<int, List<object>> aggregateValues, int index)
		{
			if (aggregateValues == null || !aggregateValues.TryGetValue(index, out List<object> valueList)) return null;

			return GetAggregatedValue(valueList.Where(v => v != null).Cast<TInput>().ToList());
		}
	}

	public abstract class ComplexAggregator : Aggregator
	{
		public IDTSInputColumnCollection100 InputColumns;

		public abstract object GetAggregatedValue(ComparableObjectList groupByValues, Dictionary<int, List<object>> aggregateValues, int index);

		public override object GetAggregatedObject(ComparableObjectList groupByValues, Dictionary<int, List<object>> aggregateValues, int index)
		{
			if (aggregateValues == null) return null;

			return GetAggregatedValue(groupByValues, aggregateValues, index);
		}
	}
}
