﻿using EFAggregate.Aggregators.Support;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator("Count")]
    public class Count<T1> : Aggregator<T1, int>
    {
        public override string GenerateDescription(string inputColumnName, string description)
        {
            return "Count of: " + inputColumnName + (!String.IsNullOrEmpty(description) ? " - " : "") + description;
        }

        public override int GetAggregatedValue(List<T1> valueList)
        {
            return valueList.Count(v => v != null);
        }

        public Count() : base()
        {
            dataProperties.length = 0;
            dataProperties.codepage = 0;
            dataProperties.precision = 0;
            dataProperties.scale = 0;
            dataProperties.type = DataType.DT_I4;
        }
    }
}
