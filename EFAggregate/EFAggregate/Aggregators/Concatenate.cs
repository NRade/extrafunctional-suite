﻿using EFAggregate.Aggregators.Support;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators
{
    [IsAggregator("Concatenate")]
    [HasArguments("; ")]
    public class Concatenate : Aggregator<string, string>
    {
        public override string GenerateDescription(string inputColumnName, string description)
        {
            return "Concatenation of: " + inputColumnName + (!String.IsNullOrEmpty(description) ? " - " : "") + description;
        }

        public override string GetAggregatedValue(List<string> valueList)
        {
            var concatenatedValue = String.Join(arguments, valueList.Where(v => v != null).Distinct());

            if (concatenatedValue.Length > 4000)
                concatenatedValue = concatenatedValue.Substring(0, 4000);

            return concatenatedValue;
        }

        public Concatenate() : base()
        {
            dataProperties.length = 4000;
        }
    }
}
