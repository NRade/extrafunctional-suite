﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators.Support
{
    class HasArguments : Attribute
    {
        public string DefaultArguments;

        public HasArguments(string defaultArguments)
        {
            DefaultArguments = defaultArguments;
        }
    }
}
