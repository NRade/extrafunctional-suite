﻿using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators.Support
{
    public class DataProperties
    {
        public DataType? type { get; set; }

        public int? length { get; set; }

        public int? precision { get; set; }

        public int? scale { get; set; }

        public int? codepage { get; set; }
    }
}
