﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFAggregate.Aggregators.Support
{
    class IsAggregator : Attribute
    {
        public string Name;

        public IsAggregator(string name)
        {
            Name = name;
        }
    }
}
