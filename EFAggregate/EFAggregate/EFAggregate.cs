﻿using EFAggregate.Aggregators;
using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFAggregate
{
	[DtsPipelineComponent(DisplayName = "EFAggregate",
		Description = "Aggregates data from multiple rows into single rows, grouping data by matching properties from specified columns.",
		UITypeName = "EFAggregate.EFAggregateComponentUI,EFAggregate,Version=1.0.0.0,Culture=neutral,PublicKeyToken=abb3e8d258cefc37",
		ComponentType = ComponentType.Transform,
		IconResource = "EFAggregate.largeIcon.ico")]
	public class EFAggregate : PipelineComponent
	{
		private bool areInputColumnsValid = false;

		private OrderedDictionary<ComparableObjectList, Dictionary<int, List<object>>> GroupedValues = new OrderedDictionary<ComparableObjectList, Dictionary<int, List<object>>>();

		private ComparableObjectList CurrentSortGroupbyValues = new ComparableObjectList();

		private int[] inputBufferColumnIndexes;
		private int[] outputBufferColumnIndexes;

		private IDTSInput100 input;
		private IDTSOutput100 output;

		private PipelineBuffer outputBuffer;

		private Aggregator[] aggregatorsByOutputColumnIndex;

		public const string AggregatorProperty = "Aggregator";
		public const string ArgumentsProperty = "Arguments";

		#region Design Time Methods

		//Design time - constructor
		public override void ProvideComponentProperties()
		{
			// Set component information
			ComponentMetaData.Name = "EFAggregate";
			ComponentMetaData.Description = "Aggregates data from multiple rows into single rows, grouping data by matching properties from specified columns.";
			ComponentMetaData.ContactInfo = "N.C. Rademaker";

			base.RemoveAllInputsOutputsAndCustomProperties();

			// Call the base class, which adds a synchronous input  
			// and output.  
			base.ProvideComponentProperties();

			// Make the output asynchronous.  
			IDTSOutput100 output = ComponentMetaData.OutputCollection[0];
			output.SynchronousInputID = 0;
			output.IsSorted = ComponentMetaData.InputCollection[0].IsSorted;
		}

		//Design time - Metadata Validator
		public override DTSValidationStatus Validate()
		{
			if (ComponentMetaData.InputCollection.Count != 1)
			{
				ComponentMetaData.FireError(0, ComponentMetaData.Name, "Incorrect number of inputs.", "", 0, out bool pbCancel);
				return DTSValidationStatus.VS_ISCORRUPT;
			}

			if (ComponentMetaData.OutputCollection.Count != 1)
			{
				ComponentMetaData.FireError(0, ComponentMetaData.Name, "Incorrect number of outputs.", "", 0, out bool pbCancel);
				return DTSValidationStatus.VS_ISCORRUPT;
			}

			input = ComponentMetaData.InputCollection[0];
			output = ComponentMetaData.OutputCollection[0];
			IDTSVirtualInput100 vInput = input.GetVirtualInput();
			var inputColumns = input.InputColumnCollection;
			var uncastOutputColumns = output.OutputColumnCollection;
			var outputColumns = uncastOutputColumns.Cast<IDTSOutputColumn100>();

			foreach (IDTSInputColumn100 column in input.InputColumnCollection)
			{
				try
				{
					IDTSVirtualInputColumn100 vColumn = vInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(column.LineageID);
				}
				catch
				{
					ComponentMetaData.FireError(0, ComponentMetaData.Name, "The input column " + column.IdentificationString + " does not match a column in the upstream component.", "", 0, out bool cancel);
					areInputColumnsValid = false;
					return DTSValidationStatus.VS_NEEDSNEWMETADATA;
				}
			}

			output.IsSorted = input.IsSorted;

			if (input.IsSorted)
			{
				var chainBroken = false;

				for (int i = 1; i < inputColumns.Count + 1; i++)
				{
					var inputColumnWithSortKey = inputColumns.Cast<IDTSInputColumn100>().FirstOrDefault(ic => Math.Abs(ic.SortKeyPosition) == i);

					if (inputColumnWithSortKey == null)
					{
						chainBroken = true;
					}

					foreach (var outputColumn in outputColumns)
					{
						var outputWithSortKeyFound = false;
						if (!chainBroken && outputColumn.MappedColumnID == inputColumnWithSortKey.ID && outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(EFAggregate.AggregatorProperty) == GroupBy.GroupByName)
						{
							outputColumn.SortKeyPosition = inputColumnWithSortKey.SortKeyPosition;
							outputWithSortKeyFound = true;
						}
						else
						{
							if (Math.Abs(outputColumn.SortKeyPosition) == i)
							{
								outputColumn.SortKeyPosition = 0;
							}
						}

						if (!outputWithSortKeyFound)
						{
							chainBroken = true;
						}
					}
				}
			}

			return DTSValidationStatus.VS_ISVALID;
		}

		public override void OnInputPathAttached(int inputID)
		{
			var vInput = input.GetVirtualInput();

			for (int i = 0; i < vInput.VirtualInputColumnCollection.Count; i++)
			{
				var vColumn = vInput.VirtualInputColumnCollection[i];

				if (!input.InputColumnCollection.Cast<IDTSInputColumn100>().Any(ic => ic.LineageID == vColumn.LineageID))
				{
					var newColumn = input.InputColumnCollection.New();
					newColumn.LineageID = vColumn.LineageID;
					newColumn.Name = vColumn.Name;

					var newOutputColumn = output.OutputColumnCollection.New();
					newOutputColumn.MappedColumnID = newColumn.ID;
					newOutputColumn.Name = newColumn.Name;
					newOutputColumn.SetDataTypeProperties(newColumn.DataType, newColumn.Length, newColumn.Precision, newColumn.Scale, newColumn.CodePage);

					newOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(AggregatorProperty, GroupBy.GroupByName);
					newOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(ArgumentsProperty, null);
				}
			}
		}

		//Design Time - method to autocorrect VS_NEEDSNEWMETADATA error
		public override void ReinitializeMetaData()
		{
			if (!areInputColumnsValid)
			{
				IDTSInput100 input = ComponentMetaData.InputCollection[0];
				IDTSVirtualInput100 vInput = input.GetVirtualInput();

				foreach (IDTSInputColumn100 column in input.InputColumnCollection)
				{
					IDTSVirtualInputColumn100 vColumn = vInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(column.LineageID);

					if (vColumn == null)
						input.InputColumnCollection.RemoveObjectByID(column.ID);
				}
				areInputColumnsValid = true;
			}

		}

		//Override InsertOutputColumnAt to prevent addition of new column from Advanced Editor
		public override IDTSOutputColumn100 InsertOutputColumnAt(
			 int outputID,
			 int outputColumnIndex,
			 string name,
			 string description)
		{
			ComponentMetaData.FireError(0, ComponentMetaData.Name, "Output columns cannot be added to " + ComponentMetaData.Name, "", 0, out bool cancel);
			//bubble-up the error to VS
			throw new Exception("Output columns cannot be added to " + ComponentMetaData.Name, null);
		}

		#endregion Design Time Methods

		#region Run Time Methods

		//Run Time - Pre Execute identifying the input columns in this component from Buffer Manager
		public override void PreExecute()
		{
			input = ComponentMetaData.InputCollection[0];
			output = ComponentMetaData.OutputCollection[0];

			inputBufferColumnIndexes = new int[input.InputColumnCollection.Count];
			for (int i = 0; i < input.InputColumnCollection.Count; i++)
			{
				inputBufferColumnIndexes[i] = BufferManager.FindColumnByLineageID(input.Buffer, input.InputColumnCollection[i].LineageID);
			}

			aggregatorsByOutputColumnIndex = new Aggregator[output.OutputColumnCollection.Count];
			outputBufferColumnIndexes = new int[output.OutputColumnCollection.Count];
			for (int i = 0; i < output.OutputColumnCollection.Count; i++)
			{
				var outputColumn = output.OutputColumnCollection[i];

				outputBufferColumnIndexes[i] = BufferManager.FindColumnByLineageID(output.Buffer, outputColumn.LineageID);

				var mappedInputColumn = input.InputColumnCollection.Cast<IDTSInputColumn100>().First(ic => ic.ID == outputColumn.MappedColumnID);

				var aggregatorName = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(AggregatorProperty);
				var arguments = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(ArgumentsProperty);

				var aggregator = Aggregator.FromString(aggregatorName, DTSExtensions.GetDNType(mappedInputColumn.DataType));

				if (aggregator is ComplexAggregator)
				{
					((ComplexAggregator)aggregator).InputColumns = input.InputColumnCollection;
				}

				aggregator.SetArguments(arguments);

				aggregatorsByOutputColumnIndex[i] = aggregator;
			}
		}

		public override void PrimeOutput(int outputs, int[] outputIDs, PipelineBuffer[] buffers)
		{
			if (buffers.Length != 0)
			{
				outputBuffer = buffers[0];
			}
		}

		//Run Time - Concatenate String Values
		public override void ProcessInput(int inputID, PipelineBuffer buffer)
		{
			if (!buffer.EndOfRowset)
			{
				GroupValues(buffer);
			}


			if (buffer.EndOfRowset)
			{
				OutputGroupedValues();

				outputBuffer.SetEndOfRowset();
			}
		}

		private void GetGroupByValues(PipelineBuffer buffer, out ComparableObjectList groupbyValues, out ComparableObjectList sortGroupbyValues)
		{
			groupbyValues = new ComparableObjectList();
			sortGroupbyValues = new ComparableObjectList();

			for (int i = 0; i < output.OutputColumnCollection.Count; i++)
			{
				var outputColumn = output.OutputColumnCollection[i];

				if (aggregatorsByOutputColumnIndex[i] is GroupBy)
				{
					var inputColumnIndex = input.InputColumnCollection.GetObjectIndexByID(outputColumn.MappedColumnID);

					groupbyValues.Add(buffer[inputBufferColumnIndexes[inputColumnIndex]]);
					if (outputColumn.SortKeyPosition != 0)
					{
						sortGroupbyValues.Add(buffer[inputBufferColumnIndexes[inputColumnIndex]]);
					}
				}
				else
				{
					groupbyValues.Add(null); // this will make indexes correspond between groupbyValues and output.OutputColumnCollection
				}
			}
		}

		private void GroupValues(PipelineBuffer buffer)
		{
			while (buffer.NextRow())
			{
				GetGroupByValues(buffer, out ComparableObjectList groupbyValues, out ComparableObjectList sortGroupbyValues);

				// if the value of any sorted column differs, we know that all already visited combinations of groupby values will not occur again in the dataset, 
				// so we can generate output already for the currently stored values. this makes output semi-synchronized
				if (!sortGroupbyValues.Equals(CurrentSortGroupbyValues))
				{
					OutputGroupedValues();
					GroupedValues = new OrderedDictionary<ComparableObjectList, Dictionary<int, List<object>>>();
					CurrentSortGroupbyValues = sortGroupbyValues;
				}

				var groupkeyEntry = GroupedValues[groupbyValues];

				for (int i = 0; i < input.InputColumnCollection.Count; i++)
				{
					groupkeyEntry.GetValueOrAddedNew(i).Add(buffer[inputBufferColumnIndexes[i]]);
				}
			}
		}

		private void OutputGroupedValues()
		{
			foreach (var keyValuePair in GroupedValues.OrderedCollection)
			{
				var valuesToGroup = keyValuePair.Key;
				var valuesToAggregate = keyValuePair.Value;

				outputBuffer.AddRow();

				for (int i = 0; i < output.OutputColumnCollection.Count; i++)
				{
					var outputColumn = output.OutputColumnCollection[i];

					if (aggregatorsByOutputColumnIndex[i] is GroupBy)
					{
						outputBuffer[outputBufferColumnIndexes[i]] = valuesToGroup[i];
					}
					else
					{
						var mappedInputIndex = input.InputColumnCollection.GetObjectIndexByID(outputColumn.MappedColumnID);

						var aggregatedObject = aggregatorsByOutputColumnIndex[i].GetAggregatedObject(keyValuePair.Key, keyValuePair.Value, mappedInputIndex);

						if (aggregatedObject != null)
						{
							outputBuffer[outputBufferColumnIndexes[i]] = aggregatedObject;
						}
					}
				}
			}
		}

		#endregion Run Time Methods
	}
}
