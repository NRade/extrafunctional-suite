﻿using EFAggregate.Aggregators;
using EFAggregate.Aggregators.Support;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace EFAggregate
{
	public partial class EFAggregateUIForm : System.Windows.Forms.Form
	{
		private Connections connections;
		private Variables variables;
		private IDTSComponentMetaData100 metaData;

		private CManagedComponentWrapper designTimeInstance;

		private CManagedComponentWrapper DesignTimeInstance
		{
			get
			{
				designTimeInstance = designTimeInstance ?? metaData.Instantiate();
				return designTimeInstance;
			}
		}
		private Button button1;
		private Button button2;
		private DataGridView outputColumnsGrid;
		private TextBox textBox1;

		private DataGridViewTextBoxColumn Datatype;
		private DataGridViewComboBoxColumn ActionType;
		private DataGridViewTextBoxColumn Arguments;
		private DataGridViewComboBoxColumn InputColumn;
		private Button button3;
		private DataGridViewTextBoxColumn OutputAlias;

		public EFAggregateUIForm(Connections cons, Variables vars, IDTSComponentMetaData100 md)
		{
			variables = vars;
			connections = cons;
			metaData = md;

			InitializeComponent();

			this.ActionType.Items.AddRange(Aggregator.GetAggregatorNames());

			this.InputColumn.Items.AddRange(metaData.GetCastInputColumns().Select(ic => ic.Name).ToArray());

			foreach (IDTSOutputColumn100 outputColumn in metaData.OutputCollection[0].OutputColumnCollection)
			{
				var aggregatorName = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(EFAggregate.AggregatorProperty);
				var arguments = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(EFAggregate.ArgumentsProperty);

				var inputColumn = metaData.GetCastInputColumns().FirstOrDefault(ic => ic.ID == outputColumn.MappedColumnID);

				if (inputColumn == null)
				{
					continue;
				}

				var lastIndex = outputColumnsGrid.Rows.Add(new string[] {
				outputColumn.DataType.ToString(),
				aggregatorName,
				arguments,
				inputColumn.Name,
				outputColumn.Name});

				var dotNetType = DTSExtensions.GetDNType(outputColumn.DataType);

				foreach (var aggregatorNameOption in Aggregator.GetAggregatorNames())
				{
					var dnType = DTSExtensions.GetDNType(inputColumn.DataType);

					var aggregatorOption = Aggregator.FromString(aggregatorNameOption, dnType);

					if (!aggregatorOption.AcceptsDataType(dnType))
					{
						var cell = (DataGridViewComboBoxCell)outputColumnsGrid.Rows[lastIndex].Cells[1];
						cell.Items.Remove(aggregatorNameOption);
					}
				}

				var aggregator = Aggregator.FromString(aggregatorName, DTSExtensions.GetDNType(inputColumn.DataType));

				if (!aggregator.GetType().GetCustomAttributes<HasArguments>().Any())
				{
					outputColumnsGrid.Rows[lastIndex].Cells[2].ReadOnly = true;
				}
			}

			var newRow = outputColumnsGrid.Rows[outputColumnsGrid.NewRowIndex];
			newRow.Cells[1].ReadOnly = true;
			newRow.Cells[2].ReadOnly = true;
			newRow.Cells[4].ReadOnly = true;

			UpdateInformationLabel();

			outputColumnsGrid.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);

			outputColumnsGrid.UserAddedRow += DataGridView1_UserAddedRow;

			outputColumnsGrid.CellValueChanged += DataGridView1_CellValueChanged;

			this.KeyDown += EFAggregateUIForm_KeyDown;
		}

		private void EFAggregateUIForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				button3_Click(button3, e);
			}
		}

		private void DataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (!suspendEvents)
			{
				suspendEvents = true;
				UpdateInformationLabel();
				suspendEvents = false;
			}
		}

		private void DataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
		{
			var currentRow = outputColumnsGrid.Rows[outputColumnsGrid.CurrentCellAddress.Y];
			currentRow.Cells[1].ReadOnly = false;
			currentRow.Cells[2].ReadOnly = true;
			currentRow.Cells[4].ReadOnly = false;

			var newRow = outputColumnsGrid.Rows[outputColumnsGrid.NewRowIndex];
			newRow.Cells[1].ReadOnly = true;
			newRow.Cells[2].ReadOnly = true;
			newRow.Cells[4].ReadOnly = true;

			UpdateInformationLabel();
		}

		private List<string> validationMessages;

		private void UpdateInformationLabel()
		{
			var informationMessage = "Warning: Output will be un-synchronized. Sort '" + GroupBy.GroupByName + "' columns to improve performance.";

			validationMessages = new List<string>();

			var groupByInputColumns = new List<string>();

			var existingOutputNames = new List<string>();

			if (outputColumnsGrid.Rows.Count == 0)
			{
				validationMessages.Add("No output columns defined.");
			}
			else
			{
				var argumentValidationMessages = new List<string>();

				for (int i = 0; i < outputColumnsGrid.Rows.Count; i++)
				{
					var row = outputColumnsGrid.Rows[i];

					if (row.IsNewRow)
					{
						continue;
					}

					var dtsType = DTSExtensions.GetDataTypeFromString((string)row.Cells[0].Value);
					var dnType = DTSExtensions.GetDNType(dtsType);
					var aggregator = Aggregator.FromString((string)row.Cells[1].Value, dnType);

					if (aggregator is ComplexAggregator)
					{
						((ComplexAggregator)aggregator).InputColumns = metaData.InputCollection[0].InputColumnCollection;
					}

					if (!aggregator.ArgumentsValid((string)row.Cells[2].Value, out string validationMessage))
					{
						argumentValidationMessages.Add("Column " + row.Cells[4].Value + ": " + validationMessage);
					}

					if ((string)row.Cells[1].Value == GroupBy.GroupByName)
					{
						var inputColumnName = (string)row.Cells[3].Value;

						if (groupByInputColumns.Contains(inputColumnName))
						{
							validationMessages.Add("Multiple '" + GroupBy.GroupByName + "' columns defined for input column '" + inputColumnName + "'.");
						}
						else
						{
							groupByInputColumns.Add(inputColumnName);
						}

						var inputColumn = metaData.GetCastInputColumns().First(ic => ic.Name == inputColumnName);

						if (inputColumn.SortKeyPosition != 0)
						{
							informationMessage = "Output will be semi-synchronized. '" + GroupBy.GroupByName + "' columns sorted.";
						}
					}

					var outputAlias = (string)row.Cells[4].Value;
					if (existingOutputNames.Contains(outputAlias))
					{
						var message = "Multiple output columns defined with the name '" + outputAlias + "'.";
						if (!validationMessages.Contains(message))
						{
							validationMessages.Add(message);
						}
					}
					else
					{
						existingOutputNames.Add(outputAlias);
					}
				}

				if (argumentValidationMessages.Any())
				{
					validationMessages.Add("Arguments invalid:");
					validationMessages.AddRange(argumentValidationMessages);
				}
			}
			if (!groupByInputColumns.Any())
			{
				validationMessages.Add("No '" + GroupBy.GroupByName + "' columns specified.");
			}

			if (validationMessages.Any())
			{
				textBox1.Text = "Invalid input: " + validationMessages.Last();
			}
			else
			{
				textBox1.Text = informationMessage;
			}
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EFAggregateUIForm));
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.outputColumnsGrid = new System.Windows.Forms.DataGridView();
			this.Datatype = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ActionType = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.Arguments = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.InputColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.OutputAlias = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.outputColumnsGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(657, 282);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(576, 282);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 3;
			this.button2.Text = "cancel";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// outputColumnsGrid
			// 
			this.outputColumnsGrid.AllowUserToResizeRows = false;
			this.outputColumnsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.outputColumnsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.outputColumnsGrid.BackgroundColor = System.Drawing.SystemColors.ControlLight;
			this.outputColumnsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.outputColumnsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Datatype,
			this.ActionType,
			this.Arguments,
			this.InputColumn,
			this.OutputAlias});
			this.outputColumnsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.outputColumnsGrid.Location = new System.Drawing.Point(15, 12);
			this.outputColumnsGrid.MultiSelect = false;
			this.outputColumnsGrid.Name = "outputColumnsGrid";
			this.outputColumnsGrid.RowHeadersVisible = false;
			this.outputColumnsGrid.Size = new System.Drawing.Size(717, 264);
			this.outputColumnsGrid.TabIndex = 4;
			// 
			// Datatype
			// 
			this.Datatype.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Datatype.HeaderText = "Input Datatype";
			this.Datatype.Name = "Datatype";
			this.Datatype.ReadOnly = true;
			this.Datatype.Width = 102;
			// 
			// ActionType
			// 
			this.ActionType.HeaderText = "Action Type";
			this.ActionType.Name = "ActionType";
			this.ActionType.Width = 70;
			// 
			// Arguments
			// 
			this.Arguments.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Arguments.HeaderText = "Arguments";
			this.Arguments.Name = "Arguments";
			this.Arguments.Width = 82;
			// 
			// InputColumn
			// 
			this.InputColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.InputColumn.HeaderText = "Input Column";
			this.InputColumn.Name = "InputColumn";
			this.InputColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.InputColumn.Width = 75;
			// 
			// OutputAlias
			// 
			this.OutputAlias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.OutputAlias.HeaderText = "Output Alias";
			this.OutputAlias.Name = "OutputAlias";
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(15, 285);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(476, 20);
			this.textBox1.TabIndex = 5;
			// 
			// button3
			// 
			this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button3.Location = new System.Drawing.Point(497, 282);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 6;
			this.button3.Text = "delete row";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// EFAggregateUIForm
			// 
			this.ClientSize = new System.Drawing.Size(744, 317);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.outputColumnsGrid);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EFAggregateUIForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "EFAggregate Editor";
			((System.ComponentModel.ISupportInitialize)(this.outputColumnsGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		private void button1_Click(object sender, EventArgs e)
		{
			var validOutputColumnIds = new List<int>();

			UpdateInformationLabel();

			if (!validationMessages.Any() || !metaData.GetCastInputColumns().Any())
			{
				foreach (DataGridViewRow row in outputColumnsGrid.Rows)
				{
					if (row.IsNewRow)
					{
						continue;
					}

					var dtsType = DTSExtensions.GetDataTypeFromString((string)row.Cells[0].Value);
					var dnType = DTSExtensions.GetDNType(dtsType);

					var existingOutputColumn = metaData.GetCastOutputColumns().FirstOrDefault(oc => oc.Name == (string)row.Cells[4].Value);

					if (existingOutputColumn == null)
					{
						existingOutputColumn = metaData.OutputCollection[0].OutputColumnCollection.New();
					}

					existingOutputColumn.Name = (string)row.Cells[4].Value;

					var mappedInputColumn = metaData.GetCastInputColumns().First(ic => ic.Name == (string)row.Cells[3].Value);
					existingOutputColumn.MappedColumnID = mappedInputColumn.ID;

					var aggregator = Aggregator.FromString((string)row.Cells[1].Value, dnType);
					aggregator.SetDataType(existingOutputColumn, mappedInputColumn);

					existingOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(EFAggregate.AggregatorProperty, aggregator.ToString());
					existingOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(EFAggregate.ArgumentsProperty, (string)row.Cells[2].Value);

					validOutputColumnIds.Add(existingOutputColumn.ID);
				}

				var invalidOutputColumnIds = new List<int>();
				foreach (IDTSOutputColumn100 outputColumn in metaData.OutputCollection[0].OutputColumnCollection)
				{
					if (!validOutputColumnIds.Contains(outputColumn.ID))
					{
						invalidOutputColumnIds.Add(outputColumn.ID);
					}
				}

				foreach (var invalidId in invalidOutputColumnIds)
				{
					metaData.OutputCollection[0].OutputColumnCollection.RemoveObjectByID(invalidId);
				}

				this.DialogResult = DialogResult.OK;

				this.Close();
			}
			else
			{
				MessageBox.Show(String.Join(Environment.NewLine, validationMessages), "Invalid input");
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;

			this.Close();
		}

		private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if ((outputColumnsGrid.CurrentCell.ColumnIndex == 1 || outputColumnsGrid.CurrentCell.ColumnIndex == 3) && e.Control is ComboBox)
			{
				ComboBox comboBox = e.Control as ComboBox;
				comboBox.SelectedIndexChanged += ComboBoxSelectionChanged;
			}
		}

		private void ComboBoxSelectionChanged(object sender, EventArgs e)
		{
			if (!suspendEvents)
			{
				suspendEvents = true;
				if (outputColumnsGrid.CurrentCellAddress.X == 1)
				{
					ActionTypeSelectionChanged(sender, e);
				}
				else if (outputColumnsGrid.CurrentCellAddress.X == 3)
				{
					InputColumnSelectionChanged(sender, e);
				}
				UpdateInformationLabel();

				suspendEvents = false;
			}
		}

		bool suspendEvents = false;

		private void InputColumnSelectionChanged(object sender, EventArgs e)
		{
			var currentcell = outputColumnsGrid.CurrentCellAddress;

			var sendingCB = sender as DataGridViewComboBoxEditingControl;

			var row = outputColumnsGrid.Rows[currentcell.Y];

			var inputColumnName = (string)row.Cells[3].Value;

			if (inputColumnName != (string)sendingCB.EditingControlFormattedValue)
			{
				inputColumnName = (string)sendingCB.EditingControlFormattedValue;

				var inputColumn = metaData.GetCastInputColumns().First(ic => ic.Name == inputColumnName);

				row.Cells[0].Value = inputColumn.DataType.ToString();

				row.Cells[4].Value = inputColumnName;

				var actionTypesCell = (DataGridViewComboBoxCell)row.Cells[1];

				actionTypesCell.Value = null;
				actionTypesCell.Items.Clear();

				var eligibleAggregatorNames = Aggregator.GetAggregatorNames().Where(aggregatorName =>
				{
					var dnType = DTSExtensions.GetDNType(inputColumn.DataType);

					var aggregator = Aggregator.FromString(aggregatorName, dnType);

					return aggregator.AcceptsDataType(dnType);
				});

				actionTypesCell.Items.AddRange(eligibleAggregatorNames.Cast<object>().ToArray());

				actionTypesCell.Value = GroupBy.GroupByName;

				var defaultArguments = Aggregator.FromString((string)actionTypesCell.Value, DTSExtensions.GetDNType(inputColumn.DataType)).GetDefaultArguments();

				row.Cells[2].Value = defaultArguments;

				row.Cells[2].ReadOnly = defaultArguments == null;

				row.Cells[3].Value = inputColumnName;
			}
		}

		private void ActionTypeSelectionChanged(object sender, EventArgs e)
		{
			var currentcell = outputColumnsGrid.CurrentCellAddress;

			var row = outputColumnsGrid.Rows[currentcell.Y];

			var selectedValue = (string)((DataGridViewComboBoxEditingControl)sender).EditingControlFormattedValue;
			if ((string)row.Cells[1].Value != selectedValue)
			{
				DataGridViewTextBoxCell argumentsCell = (DataGridViewTextBoxCell)row.Cells[2];

				var dataType = DTSExtensions.GetDNType(DTSExtensions.GetDataTypeFromString((string)row.Cells[0].Value));

				var defaultArguments = Aggregator.FromString(selectedValue, dataType).GetDefaultArguments();

				argumentsCell.Value = defaultArguments;

				argumentsCell.ReadOnly = defaultArguments == null;

				row.Cells[1].Value = selectedValue;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			if (outputColumnsGrid.CurrentCellAddress.Y != outputColumnsGrid.NewRowIndex)
			{
				outputColumnsGrid.Rows.RemoveAt(outputColumnsGrid.CurrentCellAddress.Y);

				UpdateInformationLabel();
			}
		}
	}
}
