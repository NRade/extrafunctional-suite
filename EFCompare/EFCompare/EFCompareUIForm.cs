﻿using System;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime;
using System.Collections.Generic;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using EFCompare;
using System.Reflection;

namespace EFCompare
{
    public partial class EFCompareUIForm : System.Windows.Forms.Form
    {
        private Connections connections;
        private Variables variables;
        private IDTSComponentMetaData100 metaData;

        private CManagedComponentWrapper designTimeInstance;

        private CManagedComponentWrapper DesignTimeInstance
        {
            get
            {
                designTimeInstance = designTimeInstance ?? metaData.Instantiate();
                return designTimeInstance;
            }
        }
        private Button button1;
        private Button button2;
        private DataGridView outputColumnsGrid;
        private TextBox textBox1;

        private bool suspendEvents = false;
        
        private int leftInputIndex = 0;
        private int rightInputIndex = 1;
        private int sortKeyInIndex = 2;
        private int sortKeyOutIndex = 3;
        private int outputAliasIndex = 4;
        private DataGridViewComboBoxColumn LeftInput;
        private DataGridViewComboBoxColumn RightInput;
        private DataGridViewTextBoxColumn SortKeyIn;
        private DataGridViewTextBoxColumn SortKeyOut;
        private DataGridViewTextBoxColumn OutputAlias;
        private DataGridViewTextBoxColumn OutputID;
        private Button button3;
        private CheckBox bestPairingsCheckBox;
        private ComboBox filterComboBox;
        private Label label1;
        private NumericUpDown argumentNumericUpDown;
        private int outputColumnIdIndex = 5;

        public EFCompareUIForm(Connections cons, Variables vars, IDTSComponentMetaData100 md)
        {
            variables = vars;
            connections = cons;
            metaData = md;

            InitializeComponent();
            
            var leftInputNames = metaData.InputCollection[0].InputColumnCollection.Cast<IDTSInputColumn100>().Select(ic => ToDTAndNameString(ic.DataType, ic.Name)).OrderBy(n => n).ToList();
            var rightInputNames = metaData.InputCollection[1].InputColumnCollection.Cast<IDTSInputColumn100>().Select(ic => ToDTAndNameString(ic.DataType, ic.Name)).OrderBy(n => n).ToList();

            leftInputNames.Insert(0, EFCompare.equalityCountName);
            rightInputNames.Insert(0, EFCompare.equalityCountName);
            leftInputNames.Insert(0, EFCompare.rowCountName);
            rightInputNames.Insert(0, EFCompare.rowCountName);

            LeftInput.Items.AddRange(leftInputNames.ToArray());
            RightInput.Items.AddRange(rightInputNames.ToArray());
            
            foreach (IDTSOutputColumn100 outputColumn in metaData.OutputCollection[0].OutputColumnCollection)
            {
                var isRowCount = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<bool>(EFCompare.rowCountProperty);
                var isEqualityCount = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<bool>(EFCompare.equalityCountProperty);

                if (isRowCount || isEqualityCount )
                {
                    var rowIndex = outputColumnsGrid.Rows.Add();
                    outputColumnsGrid[leftInputIndex, rowIndex].Value = isRowCount == true ? EFCompare.rowCountName : EFCompare.equalityCountName;
                    outputColumnsGrid[rightInputIndex, rowIndex].Value = isRowCount == true ? EFCompare.rowCountName : EFCompare.equalityCountName;
                    outputColumnsGrid[sortKeyInIndex, rowIndex].Value = null;
                    outputColumnsGrid[sortKeyOutIndex, rowIndex].Value = null;
                    outputColumnsGrid[outputAliasIndex, rowIndex].Value = outputColumn.Name;
                    outputColumnsGrid[outputColumnIdIndex, rowIndex].Value = outputColumn.ID;
                    
                    outputColumnsGrid[sortKeyOutIndex, rowIndex].ReadOnly = true;
                }
                else
                {
                    var leftInputColumnName = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(EFCompare.leftInputColumnProperty);
                    var rightInputColumnName = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(EFCompare.rightInputColumnProperty);

                    var leftInputColumn = metaData.InputCollection[0].InputColumnCollection.Cast<IDTSInputColumn100>().FirstOrDefault(ic => ic.Name == leftInputColumnName);
                    var rightInputColumn = metaData.InputCollection[1].InputColumnCollection.Cast<IDTSInputColumn100>().FirstOrDefault(ic => ic.Name == rightInputColumnName);

                    if(leftInputColumn == null || rightInputColumn == null)
                    {
                        var outputColumnName = outputColumn.Name;

                        continue;
                    }

                    int? sortKeyIn = null;
                    if (leftInputColumn.SortKeyPosition == rightInputColumn.SortKeyPosition)
                    {
                        sortKeyIn = leftInputColumn.SortKeyPosition;
                    }

                    var rowIndex = outputColumnsGrid.Rows.Add();
                    
                    outputColumnsGrid[leftInputIndex, rowIndex].Value = ToDTAndNameString(leftInputColumn.DataType, leftInputColumn.Name);
                    outputColumnsGrid[rightInputIndex, rowIndex].Value = ToDTAndNameString(leftInputColumn.DataType, rightInputColumn.Name);
                    outputColumnsGrid[sortKeyInIndex, rowIndex].Value = sortKeyIn;
                    outputColumnsGrid[sortKeyOutIndex, rowIndex].Value = outputColumn.SortKeyPosition;
                    outputColumnsGrid[outputAliasIndex, rowIndex].Value = outputColumn.Name;
                    outputColumnsGrid[outputColumnIdIndex, rowIndex].Value = outputColumn.ID;
                }
            }

            var newRow = outputColumnsGrid.Rows[outputColumnsGrid.NewRowIndex];
            newRow.Cells[sortKeyOutIndex].ReadOnly = true;
            newRow.Cells[outputAliasIndex].ReadOnly = true;

            bestPairingsCheckBox.Checked = metaData.GetCustomPropertyValue<bool>(EFCompare.bestPairingsOnlyProperty);

            filterComboBox.Items.AddRange(EFCompare.filters);
            filterComboBox.Text = metaData.GetCustomPropertyValue<string>(EFCompare.filterProperty);
            argumentNumericUpDown.Text = metaData.GetCustomPropertyValue<string>(EFCompare.filterArgumentProperty);

            outputColumnsGrid.CellEndEdit += OutputColumnsGrid_CellEndEdit;
            
            outputColumnsGrid.EditingControlShowing += OutputColumnsGrid_EditingControlShowing;

            outputColumnsGrid.UserAddedRow += OutputColumnsGrid_UserAddedRow;

            this.KeyDown += EFCompareUIForm_KeyDown;
            
            UpdateInformationLabel();
        }

        private void EFCompareUIForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                button3_Click(button3, e);
            }
        }

        private string ToDTAndNameString(DataType dataType, string name )
        {
            return "[" + dataType.ToString().Replace("DT_", "") + "] " + name;
        }

        private string NameFromDTAndNameString(string dtAndNameString)
        {
            var splitString = dtAndNameString.Split(new string[] { "] " }, StringSplitOptions.None);

            return splitString.Length > 1 ? splitString[1] : splitString[0];
        }

        private void OutputColumnsGrid_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            var currentRow = outputColumnsGrid.Rows[outputColumnsGrid.CurrentCellAddress.Y];
            currentRow.Cells[sortKeyOutIndex].ReadOnly = false;
            currentRow.Cells[outputAliasIndex].ReadOnly = Convert.ToString(currentRow.Cells[3].Value) == EFCompare.rowCountName;

            var newRow = outputColumnsGrid.Rows[outputColumnsGrid.NewRowIndex];
            newRow.Cells[sortKeyOutIndex].ReadOnly = true;
            newRow.Cells[outputAliasIndex].ReadOnly = true;
        }

        private void OutputColumnsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if ((outputColumnsGrid.CurrentCell.ColumnIndex == leftInputIndex || outputColumnsGrid.CurrentCell.ColumnIndex == rightInputIndex) && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                comboBox.SelectedIndexChanged += ComboBoxSelectionChanged;
            } else if(outputColumnsGrid.CurrentCell.ColumnIndex == sortKeyOutIndex)
            {
                TextBox textBox = e.Control as TextBox;
                textBox.KeyPress += TextBox_KeyPress;
            }
        }
        
        private void TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (outputColumnsGrid.CurrentCell.ColumnIndex == sortKeyOutIndex)
            {
                var textBox = (TextBox)sender;

                if (!(
                    (e.KeyChar >= '0' && e.KeyChar <= '9' && !(textBox.Text.Contains('-') && textBox.Text.IndexOf('-') >= (textBox.SelectionStart + textBox.SelectionLength)))
                    || (e.KeyChar == '-' && textBox.SelectionStart == 0 && !textBox.Text.Contains('-'))
                    || e.KeyChar == '\b'))
                {
                    e.Handled = true;
                }
            }
        }
        
        private void ComboBoxSelectionChanged(object sender, EventArgs e)
        {
            if (!suspendEvents)
            {
                suspendEvents = true;

                InputSelectionChanged(sender, e, outputColumnsGrid.CurrentCellAddress.X - leftInputIndex);
                
                UpdateInformationLabel();

                suspendEvents = false;
            }
        }
        
        private void InputSelectionChanged(object sender, EventArgs e, int leftRightIndex)
        {
            var currentRow = outputColumnsGrid.CurrentRow;

            var editingControl = (DataGridViewComboBoxEditingControl)sender;

            var thisColumnNameIndex = leftInputIndex + leftRightIndex;
            var otherColumnNameIndex = rightInputIndex - leftRightIndex;

            var formattedValue = Convert.ToString(editingControl.EditingControlFormattedValue);
            if (formattedValue == EFCompare.rowCountName || formattedValue == EFCompare.equalityCountName)
            {
                currentRow.Cells[otherColumnNameIndex].Value = formattedValue;
                currentRow.Cells[sortKeyInIndex].Value = null;
                currentRow.Cells[sortKeyOutIndex].Value = null;
                currentRow.Cells[outputAliasIndex].Value = formattedValue;
                currentRow.Cells[outputColumnIdIndex].Value = null;
                
                currentRow.Cells[sortKeyOutIndex].ReadOnly = true;
            }
            else
            {
                var inputColumn = metaData.InputCollection[leftRightIndex].InputColumnCollection.Cast<IDTSInputColumn100>().First(ic => ic.Name == NameFromDTAndNameString(Convert.ToString(editingControl.EditingControlFormattedValue)));
                
                currentRow.Cells[sortKeyInIndex].Value = inputColumn.SortKeyPosition;
                currentRow.Cells[sortKeyOutIndex].Value = inputColumn.SortKeyPosition;

                var otherInputName = NameFromDTAndNameString( Convert.ToString(currentRow.Cells[otherColumnNameIndex].Value) );

                var castOtherInputColumns = metaData.InputCollection[1 - leftRightIndex].InputColumnCollection.Cast<IDTSInputColumn100>().Where(ic => ic.DataType == inputColumn.DataType);
                var otherInputColumn = castOtherInputColumns.FirstOrDefault(ic => ic.Name == otherInputName);
                otherInputColumn = otherInputColumn ?? castOtherInputColumns.FirstOrDefault(ic => ic.Name == inputColumn.Name);
                otherInputColumn = otherInputColumn ?? castOtherInputColumns.FirstOrDefault();

                if (otherInputColumn != null)
                {
                    currentRow.Cells[otherColumnNameIndex].Value = ToDTAndNameString(otherInputColumn.DataType, otherInputColumn.Name);
                    if(otherInputColumn.Name != otherInputName)
                    {
                        currentRow.Cells[outputAliasIndex].Value = inputColumn.Name;
                    }
                } else
                {
                    currentRow.Cells[otherColumnNameIndex].Value = null;
                }
                currentRow.Cells[sortKeyOutIndex].ReadOnly = false;
            }
            currentRow.Cells[thisColumnNameIndex].Value = editingControl.EditingControlFormattedValue;
        }
        
        private List<int> sortKeysWithDatatypeMismatch = new List<int>();
        private Dictionary<int, string> sortKeyDataTypes = new Dictionary<int, string>();

        private void OutputColumnsGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            UpdateInformationLabel();
        }
        
        private List<string> validationMessages = new List<string>();

        private void UpdateInformationLabel()
        {
            validationMessages = new List<string>();

            var duplicateSortKeys = new List<int>();
            var sortKeyOuts = new List<int>();
            
            var existingNames = new List<string>();
            
            var sortKeysChanged = false;
            for (int i = 0; i < outputColumnsGrid.Rows.Count; i++)
            {
                if(i == outputColumnsGrid.NewRowIndex)
                {
                    continue;
                }

                var row = outputColumnsGrid.Rows[i];
                
                var outputAlias = (string)row.Cells[outputAliasIndex].Value;
                var sortKeyOut = Convert.ToInt32(row.Cells[sortKeyOutIndex].Value);

                if (sortKeyOut != 0)
                {
                    if (sortKeyOuts.Contains(Math.Abs(sortKeyOut)))
                    {
                        duplicateSortKeys.Add(Math.Abs(sortKeyOut));
                    }
                    else
                    {
                        sortKeyOuts.Add(Math.Abs(sortKeyOut));
                    }
                }

                if (string.IsNullOrWhiteSpace(outputAlias))
                {
                    validationMessages.Add("Output names can not be empty.");
                }
                else
                {
                    if (sortKeyOut == 0)
                    {
                        if (existingNames.Contains(outputAlias))
                        {
                            validationMessages.Add("Output column names must be unique.");
                        }
                        else
                        {
                            existingNames.Add(outputAlias);
                        }
                    }
                }

                if (Convert.ToInt32(row.Cells[sortKeyInIndex].Value) != sortKeyOut)
                {
                    sortKeysChanged = true;
                }
            }
            
            if (duplicateSortKeys.Any())
            {
                validationMessages.Add("Duplicate absolute output sort keys: " + String.Join(", ", duplicateSortKeys));
            }
            else
            {
                if (sortKeyOuts.Count == 0)
                {
                    validationMessages.Add("There should be at least one output sort key other than 0.");
                }
                else
                {
                    var orderedSortKeys = sortKeyOuts.OrderBy(s => s).ToList();
                    for (int i = 0; i < orderedSortKeys.Count - 1; i++)
                    {
                        if (orderedSortKeys[i + 1] - orderedSortKeys[i] > 1)
                        {
                            validationMessages.Add("There is a gap between the specified output sort keys.");
                            break;
                        }
                    }
                }
            }

            if (validationMessages.Any())
            {
                textBox1.Text = "Invalid input: " + validationMessages.Last();
            }
            else
            {
                if (sortKeysChanged)
                {
                    textBox1.Text = "Warning: sort keys differ between input and output. Performance will be compromised.";
                }
                else
                {
                    textBox1.Text = "Output columns with matching sort keys will be merged. Rows from different inputs will be compared where sorted values match.";
                }
            }
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EFCompareUIForm));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.outputColumnsGrid = new System.Windows.Forms.DataGridView();
            this.LeftInput = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.RightInput = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SortKeyIn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SortKeyOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OutputAlias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OutputID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.bestPairingsCheckBox = new System.Windows.Forms.CheckBox();
            this.filterComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.argumentNumericUpDown = new NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.outputColumnsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(956, 540);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(875, 540);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // outputColumnsGrid
            // 
            this.outputColumnsGrid.AllowUserToDeleteRows = false;
            this.outputColumnsGrid.AllowUserToResizeRows = false;
            this.outputColumnsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputColumnsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.outputColumnsGrid.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.outputColumnsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.outputColumnsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LeftInput,
            this.RightInput,
            this.SortKeyIn,
            this.SortKeyOut,
            this.OutputAlias,
            this.OutputID});
            this.outputColumnsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.outputColumnsGrid.Location = new System.Drawing.Point(15, 35);
            this.outputColumnsGrid.MultiSelect = false;
            this.outputColumnsGrid.Name = "outputColumnsGrid";
            this.outputColumnsGrid.RowHeadersVisible = false;
            this.outputColumnsGrid.Size = new System.Drawing.Size(1016, 499);
            this.outputColumnsGrid.TabIndex = 4;
            // 
            // LeftInput
            // 
            this.LeftInput.HeaderText = "Left Input";
            this.LeftInput.Name = "LeftInput";
            this.LeftInput.Width = 58;
            // 
            // RightInput
            // 
            this.RightInput.HeaderText = "Right Input";
            this.RightInput.Name = "RightInput";
            this.RightInput.Width = 65;
            // 
            // SortKeyIn
            // 
            this.SortKeyIn.HeaderText = "SK In";
            this.SortKeyIn.Name = "SortKeyIn";
            this.SortKeyIn.ReadOnly = true;
            this.SortKeyIn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SortKeyIn.Width = 58;
            // 
            // SortKeyOut
            // 
            this.SortKeyOut.HeaderText = "SK Out";
            this.SortKeyOut.Name = "SortKeyOut";
            this.SortKeyOut.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.SortKeyOut.Width = 66;
            // 
            // OutputAlias
            // 
            this.OutputAlias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OutputAlias.HeaderText = "Output Alias";
            this.OutputAlias.Name = "OutputAlias";
            // 
            // OutputID
            // 
            this.OutputID.HeaderText = "Output ID";
            this.OutputID.Name = "OutputID";
            this.OutputID.ReadOnly = true;
            this.OutputID.Visible = false;
            this.OutputID.Width = 78;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(15, 541);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(774, 20);
            this.textBox1.TabIndex = 5;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(795, 540);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "delete row";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // checkBox1
            // 
            this.bestPairingsCheckBox.AutoSize = true;
            this.bestPairingsCheckBox.Checked = true;
            this.bestPairingsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.bestPairingsCheckBox.Location = new System.Drawing.Point(15, 12);
            this.bestPairingsCheckBox.Name = "checkBox1";
            this.bestPairingsCheckBox.Size = new System.Drawing.Size(107, 17);
            this.bestPairingsCheckBox.TabIndex = 7;
            this.bestPairingsCheckBox.Text = "best pairings only";
            this.bestPairingsCheckBox.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.filterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filterComboBox.FormattingEnabled = true;
            this.filterComboBox.Location = new System.Drawing.Point(192, 10);
            this.filterComboBox.Name = "filterComboBox";
            this.filterComboBox.Size = new System.Drawing.Size(173, 21);
            this.filterComboBox.TabIndex = 8;
            this.filterComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(128, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "filter mode:";
            // 
            // textBox2
            // 
            this.argumentNumericUpDown.Location = new System.Drawing.Point(371, 10);
            this.argumentNumericUpDown.Name = "argumentNumericUpDown";
            this.argumentNumericUpDown.Size = new System.Drawing.Size(173, 20);
            this.argumentNumericUpDown.TabIndex = 10;
            // 
            // EFCompareUIForm
            // 
            this.ClientSize = new System.Drawing.Size(1043, 575);
            this.Controls.Add(this.argumentNumericUpDown);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filterComboBox);
            this.Controls.Add(this.bestPairingsCheckBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.outputColumnsGrid);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EFCompareUIForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "EFCompare Editor";
            ((System.ComponentModel.ISupportInitialize)(this.outputColumnsGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!validationMessages.Any())
            {
                var castLeftInputColumns = metaData.InputCollection[0].InputColumnCollection.Cast<IDTSInputColumn100>();
                var castRightInputColumns = metaData.InputCollection[1].InputColumnCollection.Cast<IDTSInputColumn100>();
                var castOutputColumns = metaData.OutputCollection[0].OutputColumnCollection.Cast<IDTSOutputColumn100>();

                var validOutputColumnIds = new List<int>();

                foreach (DataGridViewRow row in outputColumnsGrid.Rows)
                {
                    if(row.IsNewRow)
                    {
                        continue;
                    }

                    var outputColumn = castOutputColumns.FirstOrDefault(oc => oc.ID == Convert.ToInt32(row.Cells[outputColumnIdIndex].Value));
                    
                    if(outputColumn == null)
                    {
                        outputColumn = metaData.OutputCollection[0].OutputColumnCollection.New();
                        outputColumn.SetDataTypeProperties(DataType.DT_BOOL, 0, 0, 0, 0);
                    }

                    outputColumn.Name = Convert.ToString(row.Cells[outputAliasIndex].Value);

                    if ((string)row.Cells[leftInputIndex].Value == EFCompare.rowCountName)
                    {
                        outputColumn.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.rowCountProperty, true);
                    } else if ((string)row.Cells[leftInputIndex].Value == EFCompare.equalityCountName)
                    {
                        outputColumn.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.equalityCountProperty, true);
                    }
                    else
                    {
                        outputColumn.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.columnCompareProperty, Convert.ToInt32(row.Cells[sortKeyOutIndex].Value) == 0);

                        var leftInputColumn = castLeftInputColumns.FirstOrDefault(ic => ic.Name == NameFromDTAndNameString(Convert.ToString(row.Cells[leftInputIndex].Value)));
                        var rightInputColumn = castRightInputColumns.FirstOrDefault(ic => ic.Name == NameFromDTAndNameString(Convert.ToString(row.Cells[rightInputIndex].Value)));

                        outputColumn.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.leftInputColumnProperty, leftInputColumn.Name);
                        outputColumn.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.rightInputColumnProperty, rightInputColumn.Name);
                    }

                    validOutputColumnIds.Add(outputColumn.ID);
                }

                var invalidOutputColumnIds = castOutputColumns.Select(oc => oc.ID).Where(oc => !validOutputColumnIds.Contains(oc)).ToList();
                foreach (var invalidOutputColumnId in invalidOutputColumnIds)
                {
                    metaData.OutputCollection[0].OutputColumnCollection.RemoveObjectByID(invalidOutputColumnId);
                }

                metaData.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.bestPairingsOnlyProperty, bestPairingsCheckBox.Checked);
                metaData.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.filterProperty, filterComboBox.Text);
                metaData.CustomPropertyCollection.SetCustomPropertyValue(EFCompare.filterArgumentProperty, argumentNumericUpDown.Text);

                this.DialogResult = DialogResult.OK;

                this.Close();
            }
            else
            {
                MessageBox.Show(String.Join(Environment.NewLine, validationMessages.Distinct()), "Invalid input");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (outputColumnsGrid.CurrentCellAddress.Y != outputColumnsGrid.NewRowIndex)
            {
                outputColumnsGrid.Rows.RemoveAt(outputColumnsGrid.CurrentCellAddress.Y);

                UpdateInformationLabel();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(((ComboBox)sender).Text)
            {
                case EFCompare.differencesFilter:
                    argumentNumericUpDown.Text = "";
                    argumentNumericUpDown.Enabled = false;
                    break;
                case EFCompare.matchesFilter:
                    argumentNumericUpDown.Text = "";
                    argumentNumericUpDown.Enabled = false;
                    break;
                case EFCompare.aboveEqualityCountFilter:
                    argumentNumericUpDown.Text = "5";
                    argumentNumericUpDown.Enabled = true;
                    break;
                case EFCompare.belowEqualityCountFilter:
                    argumentNumericUpDown.Text = "5";
                    argumentNumericUpDown.Enabled = true;
                    break;
                case EFCompare.aboveInequalityCountFilter:
                    argumentNumericUpDown.Text = "5";
                    argumentNumericUpDown.Enabled = true;
                    break;
                case EFCompare.belowInequalityCountFilter:
                    argumentNumericUpDown.Text = "5";
                    argumentNumericUpDown.Enabled = true;
                    break;
                case EFCompare.aboveEqualityPercentageFilter:
                    argumentNumericUpDown.Text = "50";
                    argumentNumericUpDown.Enabled = true;
                    break;
                case EFCompare.belowEqualityPercentageFilter:
                    argumentNumericUpDown.Text = "50";
                    argumentNumericUpDown.Enabled = true;
                    break;
                case EFCompare.outliersByCountFilter:
                    argumentNumericUpDown.Text = "5";
                    argumentNumericUpDown.Enabled = true;
                    break;
                case EFCompare.outliersByPercentageFilter:
                    argumentNumericUpDown.Text = "10";
                    argumentNumericUpDown.Enabled = true;
                    break;
                default:
                    argumentNumericUpDown.Text = "";
                    argumentNumericUpDown.Enabled = false;
                    break;
            }
        }
    }
}
