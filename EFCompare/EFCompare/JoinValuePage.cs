﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCompare
{
    class JoinValuePage
    {
        public InputIndexPage[] InputIndexPages;

        public JoinValuePage(int inputCount)
        {
            InputIndexPages = new InputIndexPage[inputCount];
            for(int i = 0; i < inputCount; i++)
            {
                InputIndexPages[i] = new InputIndexPage();
            }
        }
        
        public void AddCurrentValues(int inputIndex, object[] currentValues)
        {
            InputIndexPages[inputIndex].CurrentValues.Add(currentValues);
        }
    }
}
