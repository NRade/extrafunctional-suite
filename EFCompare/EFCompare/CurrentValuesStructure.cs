﻿using System.Collections.Generic;
using System.Linq;

namespace EFCompare
{
	interface CurrentValuesStructure
	{
		void AddCurrentValues(object joinValue, int inputIndex, object[] currentValues);

		bool ExtractFirstInLineValuesIfReady(out List<object[]>[] inputIndexPages);

		bool HasJoinValuePages();

		bool AllInputsFinished();

		void SetInputFinished(int inputIndex);
	}

	class CurrentValuesStructure<T> : CurrentValuesStructure
	{
		private SortedList<T, List<object[]>[]> JoinValuePages = new SortedList<T, List<object[]>[]>();

		private int inputCount;

		private InputCurrentJoinValue<T>[] inputCurrentJoinValues;

		public CurrentValuesStructure(int inputCount)
		{
			this.inputCount = inputCount;
			inputCurrentJoinValues = new InputCurrentJoinValue<T>[inputCount];
			for (int i = 0; i < inputCurrentJoinValues.Length; i++)
			{
				inputCurrentJoinValues[i] = new InputCurrentJoinValue<T>();
			}
		}

		public List<int> InputsFinished = new List<int>();

		public void AddCurrentValues(object joinValue, int inputIndex, object[] currentValues)
		{
			if (!JoinValuePages.TryGetValue((T)joinValue, out List<object[]>[] existingJoinValuePage))
			{
				existingJoinValuePage = new List<object[]>[inputCount];
				for (int i = 0; i < inputCount; i++)
				{
					existingJoinValuePage[i] = new List<object[]>();
				}
				JoinValuePages.Add((T)joinValue, existingJoinValuePage);
			}

			existingJoinValuePage[inputIndex].Add(currentValues);

			inputCurrentJoinValues[inputIndex].JoinValue = joinValue;
		}

		public bool ExtractFirstInLineValuesIfReady(out List<object[]>[] inputIndexPages)
		{
			var firstKvp = JoinValuePages.First();
			if (!inputCurrentJoinValues.All(i => i.InputAheadOf(firstKvp.Key)))
			{
				inputIndexPages = null;
				return false;
			}
			inputIndexPages = firstKvp.Value;
			JoinValuePages.Remove(firstKvp.Key);
			return true;
		}

		public bool HasJoinValuePages()
		{
			return JoinValuePages.Any();
		}

		public bool AllInputsFinished()
		{
			return inputCurrentJoinValues.All(i => i.State == InputState.Finished);
		}

		public void SetInputFinished(int inputIndex)
		{
			inputCurrentJoinValues[inputIndex].SetFinished();
		}
	}
}
