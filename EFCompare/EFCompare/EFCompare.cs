﻿using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFCompare
{
	[DtsPipelineComponent(DisplayName = "EFCompare",
		Description = "Compares the contents of dataflows and provides information on their differences.",
		UITypeName = "EFCompare.EFCompareComponentUI,EFCompare,Version=1.0.0.0,Culture=neutral,PublicKeyToken=379b12d344668ef2",
		ComponentType = ComponentType.Transform,
		IconResource = "EFCompare.EFCompareIconLarge.ico")]
	public class EFCompare : PipelineComponent
	{
		private int[][] inputBufferColumnIndexes;
		private int[] outputBufferColumnIndexes;

		CurrentValuesStructure CurrentValueStructure;

		List<KeyValuePair<bool, int>>[] joinIndexesByInputIndex;

		private bool sortKeyPositionsChanged = false;

		private PipelineBuffer outputBuffer;

		private IDTSOutput100 output;

		public const string rowCountProperty = "rowCount";
		public const string equalityCountProperty = "equalityCount";
		public const string columnCompareProperty = "columnCompare";
		public const string leftInputColumnProperty = "leftInputColumnID";
		public const string rightInputColumnProperty = "rightInputColumnID";
		public const string inputColumnsValidProperty = "inputColumnsValid";

		public const string rowCountName = "Row count";
		public const string equalityCountName = "Equality count";

		private bool bestPairingsOnly = true;

		public const string bestPairingsOnlyProperty = "bestPairingsOnly";
		public const string filterProperty = "filter";
		public const string filterArgumentProperty = "filterArgument";

		public static string[] filters = { showAllFilter, differencesFilter, matchesFilter, aboveEqualityCountFilter, belowEqualityCountFilter, aboveInequalityCountFilter, belowInequalityCountFilter, aboveEqualityPercentageFilter, belowEqualityPercentageFilter, outliersByCountFilter, outliersByPercentageFilter };

		public const string showAllFilter = "show all";
		public const string differencesFilter = "differences";
		public const string matchesFilter = "matches";
		public const string aboveEqualityCountFilter = "above equality count";
		public const string belowEqualityCountFilter = "below equality count";
		public const string aboveInequalityCountFilter = "above inequality count";
		public const string belowInequalityCountFilter = "below inequality count";
		public const string aboveEqualityPercentageFilter = "above equality %";
		public const string belowEqualityPercentageFilter = "below equality %";
		public const string outliersByCountFilter = "outliers by count";
		public const string outliersByPercentageFilter = "outliers by %";

		#region Design Time Methods

		//Design time - constructor
		public override void ProvideComponentProperties()
		{
			// Set component information
			ComponentMetaData.Name = "EFCompare";
			ComponentMetaData.Description = "Compares the contents of dataflows and provides information on their differences.";
			ComponentMetaData.ContactInfo = "N.C. Rademaker";

			base.RemoveAllInputsOutputsAndCustomProperties();

			IDTSInput100 leftInput = ComponentMetaData.InputCollection.New();
			leftInput.Name = "Left Input";

			IDTSInput100 rightInput = ComponentMetaData.InputCollection.New();
			rightInput.Name = "Right Input";

			IDTSOutput100 output = ComponentMetaData.OutputCollection.New();
			output.Name = "Output";
			output.SynchronousInputID = 0;
			output.IsSorted = true;

			ComponentMetaData.CustomPropertyCollection.SetCustomPropertyValue(bestPairingsOnlyProperty, true);
			ComponentMetaData.CustomPropertyCollection.SetCustomPropertyValue(filterProperty, showAllFilter);
			ComponentMetaData.CustomPropertyCollection.SetCustomPropertyValue(filterArgumentProperty, "");
		}

		//Design time - Metadata Validator
		public override DTSValidationStatus Validate()
		{
			if (ComponentMetaData.OutputCollection.Count != 1)
			{
				ComponentMetaData.FireError(0, ComponentMetaData.Name, "Incorrect number of outputs.", "", 0, out bool pbCancel);
				return DTSValidationStatus.VS_ISCORRUPT;
			}

			return DTSValidationStatus.VS_ISVALID;
		}

		//Design Time - method to autocorrect VS_NEEDSNEWMETADATA error
		public override void ReinitializeMetaData()
		{
			foreach (IDTSInput100 input in ComponentMetaData.InputCollection)
			{
				var areInputColumnsValid = input.CustomPropertyCollection.GetCustomPropertyOrCreate(inputColumnsValidProperty, false);
				if (!(bool)areInputColumnsValid.Value)
				{
					IDTSVirtualInput100 vInput = input.GetVirtualInput();

					foreach (IDTSInputColumn100 column in input.InputColumnCollection)
					{
						IDTSVirtualInputColumn100 vColumn = vInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(column.LineageID);

						if (vColumn == null)
							input.InputColumnCollection.RemoveObjectByID(column.ID);
					}
					areInputColumnsValid.Value = true;
				}
			}
		}

		public override void OnInputPathAttached(int inputID)
		{
			var input = ComponentMetaData.InputCollection.GetObjectByID(inputID);
			var vInput = input.GetVirtualInput();

			foreach (IDTSVirtualInputColumn100 vColumn in vInput.VirtualInputColumnCollection)
			{
				var existingInputColumn = input.InputColumnCollection.Cast<IDTSInputColumn100>().FirstOrDefault(ic => ic.LineageID == vColumn.LineageID);

				if (existingInputColumn == null)
				{
					existingInputColumn = input.InputColumnCollection.New();
					existingInputColumn.LineageID = vColumn.LineageID;
				}
				existingInputColumn.Name = vColumn.Name;
				existingInputColumn.Description = vColumn.Description;
			}

			if (ComponentMetaData.InputCollection[0].IsAttached && ComponentMetaData.InputCollection[1].IsAttached)
			{
				ComponentMetaData.OutputCollection[0].OutputColumnCollection.RemoveAll();

				var rowCountOutputColumn = ComponentMetaData.OutputCollection[0].OutputColumnCollection.New();
				rowCountOutputColumn.Name = rowCountName;
				rowCountOutputColumn.SetDataTypeProperties(DataType.DT_WSTR, 200, 0, 0, 0);

				rowCountOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(equalityCountProperty, false);
				rowCountOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(rowCountProperty, true);

				var equalityCountOutputColumn = ComponentMetaData.OutputCollection[0].OutputColumnCollection.New();
				equalityCountOutputColumn.Name = equalityCountName;
				equalityCountOutputColumn.SetDataTypeProperties(DataType.DT_WSTR, 200, 0, 0, 0);

				equalityCountOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(rowCountProperty, false);
				equalityCountOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(equalityCountProperty, true);

				var castInputColumns = ComponentMetaData.InputCollection[0].InputColumnCollection.Cast<IDTSInputColumn100>();
				var orderedInputColumns = castInputColumns.OrderBy(ic => ic.SortKeyPosition == 0).ThenBy(ic => Math.Abs(ic.SortKeyPosition));

				foreach (IDTSInputColumn100 leftInputColumn in orderedInputColumns)
				{
					foreach (IDTSInputColumn100 rightInputColumn in ComponentMetaData.InputCollection[1].InputColumnCollection)
					{
						if (leftInputColumn.Name == rightInputColumn.Name)
						{
							var newOutputColumn = ComponentMetaData.OutputCollection[0].OutputColumnCollection.New();
							newOutputColumn.Name = leftInputColumn.Name;

							var length = Math.Max(200, Math.Min(4000, leftInputColumn.Length + rightInputColumn.Length + 20));

							newOutputColumn.SetDataTypeProperties(DataType.DT_WSTR, length, 0, 0, 0);

							if (leftInputColumn.SortKeyPosition == rightInputColumn.SortKeyPosition)
							{
								newOutputColumn.SortKeyPosition = leftInputColumn.SortKeyPosition;
							}

							newOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(rowCountProperty, false);
							newOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(equalityCountProperty, false);
							newOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(columnCompareProperty, newOutputColumn.SortKeyPosition == 0);

							newOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(leftInputColumnProperty, leftInputColumn.Name);
							newOutputColumn.CustomPropertyCollection.SetCustomPropertyValue(rightInputColumnProperty, rightInputColumn.Name);
						}
					}
				}
			}
		}

		//Override InsertOutputColumnAt to prevent addition of new column from Advanced Editor
		public override IDTSOutputColumn100 InsertOutputColumnAt(
			 int outputID,
			 int outputColumnIndex,
			 string name,
			 string description)
		{
			ComponentMetaData.FireError(0, ComponentMetaData.Name, "Output columns cannot be added to " + ComponentMetaData.Name, "", 0, out bool cancel);
			//bubble-up the error to VS
			throw new Exception("Output columns cannot be added to " + ComponentMetaData.Name, null);
		}

		#endregion Design Time Methods

		#region Run Time Methods

		private int columnCompareCount = -1;

		private string filterType;
		private string filterArgument;

		//Run Time - Pre Execute identifying the input columns in this component from Buffer Manager
		public override void PreExecute()
		{
			sortKeyPositionsChanged = false;

			output = ComponentMetaData.OutputCollection[0];

			joinIndexesByInputIndex = new List<KeyValuePair<bool, int>>[] {
					new List<KeyValuePair<bool, int>>(),
					new List<KeyValuePair<bool, int>>()
				};

			OutputColumnProperties = new Dictionary<int, Dictionary<string, object>>();

			bestPairingsOnly = ComponentMetaData.GetCustomPropertyValue<bool>(bestPairingsOnlyProperty);
			filterType = ComponentMetaData.GetCustomPropertyValue<string>(filterProperty);
			filterArgument = ComponentMetaData.GetCustomPropertyValue<string>(filterArgumentProperty);

			CurrentValueStructure = new CurrentValuesStructure<SortableObjectList>(ComponentMetaData.InputCollection.Count);

			inputBufferColumnIndexes = new int[ComponentMetaData.InputCollection.Count][];
			for (int h = 0; h < ComponentMetaData.InputCollection.Count; h++)
			{
				var input = ComponentMetaData.InputCollection[h];

				var joinIndexesTemp = new SortedList<int, KeyValuePair<bool, int>>();

				inputBufferColumnIndexes[h] = new int[input.InputColumnCollection.Count];
				for (int i = 0; i < input.InputColumnCollection.Count; i++)
				{
					inputBufferColumnIndexes[h][i] = BufferManager.FindColumnByLineageID(input.Buffer, input.InputColumnCollection[i].LineageID);
				}
			}

			var leftJoinIndexes = new SortedList<int, KeyValuePair<bool, int>>();
			var rightJoinIndexes = new SortedList<int, KeyValuePair<bool, int>>();

			var leftInputColumns = ComponentMetaData.InputCollection[0].InputColumnCollection;
			var rightInputColumns = ComponentMetaData.InputCollection[1].InputColumnCollection;

			outputBufferColumnIndexes = new int[output.OutputColumnCollection.Count];
			for (int i = 0; i < output.OutputColumnCollection.Count; i++)
			{
				var outputColumn = output.OutputColumnCollection[i];

				var customPropertiesDict = new Dictionary<string, object>();
				OutputColumnProperties.Add(outputColumn.ID, customPropertiesDict);
				foreach (IDTSCustomProperty100 customProperty in outputColumn.CustomPropertyCollection)
				{
					customPropertiesDict.Add(customProperty.Name, customProperty.Value);
				}

				outputBufferColumnIndexes[i] = BufferManager.FindColumnByLineageID(output.Buffer, output.OutputColumnCollection[i].LineageID);

				if (outputColumn.SortKeyPosition != 0)
				{
					var leftInputName = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(leftInputColumnProperty);
					var rightInputName = outputColumn.CustomPropertyCollection.GetCustomPropertyValue<string>(rightInputColumnProperty);

					var leftInputColumn = leftInputColumns.Cast<IDTSInputColumn100>().FirstOrDefault(ic => ic.Name == leftInputName);
					var rightInputColumn = rightInputColumns.Cast<IDTSInputColumn100>().FirstOrDefault(ic => ic.Name == rightInputName);

					var leftColumnIndex = leftInputColumns.FindObjectIndexByID(leftInputColumn.ID);
					var rightColumnIndex = rightInputColumns.FindObjectIndexByID(rightInputColumn.ID);

					leftJoinIndexes.Add(Math.Abs(outputColumn.SortKeyPosition), new KeyValuePair<bool, int>(outputColumn.SortKeyPosition < 0, leftColumnIndex));
					rightJoinIndexes.Add(Math.Abs(outputColumn.SortKeyPosition), new KeyValuePair<bool, int>(outputColumn.SortKeyPosition < 0, rightColumnIndex));

					if (leftInputColumn.SortKeyPosition != outputColumn.SortKeyPosition || rightInputColumn.SortKeyPosition != outputColumn.SortKeyPosition)
					{
						sortKeyPositionsChanged = true;
					}
				}
			}

			joinIndexesByInputIndex[0].AddRange(leftJoinIndexes.Values);
			joinIndexesByInputIndex[1].AddRange(rightJoinIndexes.Values);

			columnCompareCount = ComponentMetaData.OutputCollection[0].OutputColumnCollection.Cast<IDTSOutputColumn100>().Count(oc => oc.CustomPropertyCollection.GetCustomPropertyValue<bool>(columnCompareProperty));
		}

		public override void PrimeOutput(int outputs, int[] outputIDs, PipelineBuffer[] buffers)
		{
			if (buffers.Any())
			{
				outputBuffer = buffers[0];
			}
		}

		public override void ProcessInput(int inputID, PipelineBuffer buffer)
		{
			if (!buffer.EndOfRowset)
			{
				while (buffer.NextRow())
				{
					var inputIndex = ComponentMetaData.InputCollection.FindObjectIndexByID(inputID);
					var input = ComponentMetaData.InputCollection[inputIndex];

					var joinValue = new SortableObjectList();
					foreach (var kvp in joinIndexesByInputIndex[inputIndex])
					{
						joinValue.Add((IComparable)buffer[inputBufferColumnIndexes[inputIndex][kvp.Value]], kvp.Key);
					}

					var values = new object[input.InputColumnCollection.Count];
					for (int inputColumnIndex = 0; inputColumnIndex < input.InputColumnCollection.Count; inputColumnIndex++)
					{
						values[inputColumnIndex] = buffer[inputBufferColumnIndexes[inputIndex][inputColumnIndex]];
					}

					CurrentValueStructure.AddCurrentValues(joinValue, inputIndex, values);

					if (!sortKeyPositionsChanged)
					{
						if (CurrentValueStructure.ExtractFirstInLineValuesIfReady(out List<object[]>[] valuesByInputIndex))
						{
							OutputComparison(valuesByInputIndex);
						}
					}
				}
			}
			else
			{
				var inputIndex = ComponentMetaData.InputCollection.FindObjectIndexByID(inputID);

				CurrentValueStructure.SetInputFinished(inputIndex);

				while (CurrentValueStructure.HasJoinValuePages() && CurrentValueStructure.ExtractFirstInLineValuesIfReady(out List<object[]>[] valuesByInputIndex))
				{
					OutputComparison(valuesByInputIndex);
				}

				if (CurrentValueStructure.AllInputsFinished())
				{
					outputBuffer.SetEndOfRowset();
				}
			}
		}

		Dictionary<int, Dictionary<string, object>> OutputColumnProperties = new Dictionary<int, Dictionary<string, object>>();

		private void OutputComparison(List<object[]>[] inputIndexPages)
		{
			var leftValueList = inputIndexPages[0];
			var rightValueList = inputIndexPages[1];

			var originalLeftCount = leftValueList.Count;
			var originalRightCount = rightValueList.Count;

			if (leftValueList.Count == 0)
			{
				leftValueList.Add(new object[ComponentMetaData.InputCollection[0].InputColumnCollection.Count]);
			}

			if (rightValueList.Count == 0)
			{
				rightValueList.Add(new object[ComponentMetaData.InputCollection[0].InputColumnCollection.Count]);
			}

			var outputValuesMatrix = new List<List<KeyValuePair<int, object[]>>>();

			foreach (var leftValues in leftValueList)
			{
				var leftOutputValuesList = new List<KeyValuePair<int, object[]>>();
				outputValuesMatrix.Add(leftOutputValuesList);

				foreach (var rightValues in rightValueList)
				{
					var equalCount = 0;
					var outputValues = new object[output.OutputColumnCollection.Count];

					for (int i = 0; i < output.OutputColumnCollection.Count; i++)
					{
						var outputColumn = output.OutputColumnCollection[i];

						if ((bool)OutputColumnProperties[outputColumn.ID][equalityCountProperty] == true)
						{
							continue;
						}

						if ((bool)OutputColumnProperties[outputColumn.ID][rowCountProperty] == true)
						{
							string symbolPrefix;
							if (originalLeftCount == originalRightCount)
							{
								symbolPrefix = "[==] ";
							}
							else if (originalLeftCount > originalRightCount)
							{
								symbolPrefix = "[>] ";
							}
							else
							{
								symbolPrefix = "[<] ";
							}

							outputValues[i] = symbolPrefix + originalLeftCount + ":" + originalRightCount;
						}
						else
						{
							if (originalLeftCount > 0 && originalRightCount > 0)
							{
								var leftName = (string)OutputColumnProperties[outputColumn.ID][leftInputColumnProperty];
								var rightName = (string)OutputColumnProperties[outputColumn.ID][rightInputColumnProperty];

								var leftColumnIndex = -1;
								for (int j = 0; j < ComponentMetaData.InputCollection[0].InputColumnCollection.Count; j++)
								{
									if (ComponentMetaData.InputCollection[0].InputColumnCollection[j].Name == leftName)
									{
										leftColumnIndex = j;
										break;
									}
								}

								var rightColumnIndex = -1;
								for (int j = 0; j < ComponentMetaData.InputCollection[1].InputColumnCollection.Count; j++)
								{
									if (ComponentMetaData.InputCollection[1].InputColumnCollection[j].Name == rightName)
									{
										rightColumnIndex = j;
										break;
									}
								}

								var leftValue = leftValues[leftColumnIndex];
								var rightValue = rightValues[rightColumnIndex];

								if (outputColumn.SortKeyPosition != 0)
								{
									outputValues[i] = leftValue;
								}
								else
								{
									if (leftValue == null && rightValue != null)
									{
										outputValues[i] = "[!=] Left is null.";
									}
									else if (leftValue != null && rightValue == null)
									{
										outputValues[i] = "[!=] Right is null.";
									}
									else if (leftValue == null && rightValue == null)
									{
										outputValues[i] = "[==] Both null.";
										equalCount++;
									}
									else if (leftValue is string)
									{
										if ((string)leftValue == (string)rightValue)
										{
											outputValues[i] = "[==]";
											equalCount++;
										}
										else
										{
											outputValues[i] = GetDiffText((string)leftValue, (string)rightValue);
										}
									}
									else if (leftValue is IComparable)
									{
										var leftValueComparable = ((IComparable)leftValue);
										var rightValueComparable = ((IComparable)rightValue);

										var comparisonNumber = leftValueComparable.CompareTo(rightValueComparable);

										if (comparisonNumber == 0)
										{
											outputValues[i] = "[==]";
											equalCount++;
										}
										else if (comparisonNumber > 0)
										{
											outputValues[i] = "[>]";
										}
										else
										{
											outputValues[i] = "[<]";
										}
									}
									else
									{
										if (Convert.ToString(leftValue) == Convert.ToString(rightValue))
										{
											outputValues[i] = "[==] " + Convert.ToString(leftValue);
											equalCount++;
										}
										else
										{
											outputValues[i] = GetDiffText(Convert.ToString(leftValue), Convert.ToString(rightValue));
										}
									}
								}
							}
						}
					}

					leftOutputValuesList.Add(new KeyValuePair<int, object[]>(equalCount, outputValues));
				}
			}

			if (bestPairingsOnly)
			{
				if (!(FoundCombinations.TryGetValue(outputValuesMatrix.Count, out Dictionary<int, List<List<KeyValuePair<int, int>>>> innerDictionary)
					&& innerDictionary.TryGetValue(outputValuesMatrix[0].Count, out List<List<KeyValuePair<int, int>>> allCombinations)))
				{
					allCombinations = FindCombinations(outputValuesMatrix, new List<KeyValuePair<int, int>>());
					innerDictionary = new Dictionary<int, List<List<KeyValuePair<int, int>>>>();
					innerDictionary.Add(outputValuesMatrix[0].Count, allCombinations);
					FoundCombinations.Add(outputValuesMatrix.Count, innerDictionary);
				}

				var bestEqualCount = -1;
				List<KeyValuePair<int, int>> bestCombination = null;

				foreach (var combination in allCombinations)
				{
					var equalCount = 0;
					foreach (var pairing in combination)
					{
						equalCount += outputValuesMatrix[pairing.Key][pairing.Value].Key;
					}

					if (equalCount > bestEqualCount)
					{
						bestEqualCount = equalCount;
						bestCombination = combination;
					}
				}

				foreach (var pairing in bestCombination)
				{
					if (PassesFilter(outputValuesMatrix[pairing.Key][pairing.Value].Key, out string symbolPrefix))
					{
						outputBuffer.AddRow();

						var objectArray = outputValuesMatrix[pairing.Key][pairing.Value].Value;

						for (int i = 0; i < objectArray.Length; i++)
						{
							if ((bool)OutputColumnProperties[ComponentMetaData.OutputCollection[0].OutputColumnCollection[i].ID][equalityCountProperty] == true)
							{
								outputBuffer[outputBufferColumnIndexes[i]] = symbolPrefix + outputValuesMatrix[pairing.Key][pairing.Value].Key + "/" + columnCompareCount;
							}
							else
							{
								outputBuffer[outputBufferColumnIndexes[i]] = objectArray[i];
							}
						}
					}
				}
			}
			else
			{
				for (int i = 0; i < outputValuesMatrix.Count; i++)
				{
					for (int j = 0; j < outputValuesMatrix[i].Count; j++)
					{
						if (PassesFilter(outputValuesMatrix[i][j].Key, out string symbolPrefix))
						{
							outputBuffer.AddRow();

							var objectArray = outputValuesMatrix[i][j].Value;

							for (int k = 0; k < objectArray.Length; k++)
							{
								if ((bool)OutputColumnProperties[ComponentMetaData.OutputCollection[0].OutputColumnCollection[k].ID][equalityCountProperty] == true)
								{
									outputBuffer[outputBufferColumnIndexes[k]] = symbolPrefix + outputValuesMatrix[i][j].Key + "/" + columnCompareCount;
								}
								else
								{
									outputBuffer[outputBufferColumnIndexes[k]] = objectArray[k];
								}
							}
						}
					}
				}
			}

		}

		private bool PassesFilter(int equalityCount, out string symbolPrefix)
		{
			if (equalityCount == columnCompareCount)
			{
				symbolPrefix = "[==] ";
			}
			else
			{
				symbolPrefix = "[!=] ";
			}

			switch (filterType)
			{
				case differencesFilter:
					return equalityCount != columnCompareCount;
				case matchesFilter:
					return equalityCount == columnCompareCount;
				case aboveEqualityCountFilter:
					return equalityCount > Convert.ToInt32(filterArgument);
				case belowEqualityCountFilter:
					return equalityCount < Convert.ToInt32(filterArgument);
				case aboveInequalityCountFilter:
					var inequalityCount = columnCompareCount - equalityCount;
					return inequalityCount > Convert.ToInt32(filterArgument);
				case belowInequalityCountFilter:
					var inequalityCount2 = columnCompareCount - equalityCount;
					return inequalityCount2 < Convert.ToInt32(filterArgument);
				case aboveEqualityPercentageFilter:
					return (equalityCount / columnCompareCount) > (Convert.ToInt32(filterArgument) / 100);
				case belowEqualityPercentageFilter:
					return (equalityCount / columnCompareCount) < (Convert.ToInt32(filterArgument) / 100);
				case outliersByCountFilter:
					return equalityCount < Convert.ToInt32(filterArgument) || equalityCount > columnCompareCount - Convert.ToInt32(filterArgument);
				case outliersByPercentageFilter:
					var percentage = (equalityCount / columnCompareCount);
					var argumentPercentage = (Convert.ToInt32(filterArgument) / 100);
					return percentage < argumentPercentage || percentage > 100 - argumentPercentage;
				default:
					return true;
			}
		}

		private string GetDiffText(string leftValue, string rightValue)
		{
			var dmp = new DiffMatchPatch.diff_match_patch();
			var diff = dmp.diff_main(leftValue, rightValue);
			dmp.diff_cleanupSemantic(diff);

			return "[!=] " + dmp.patch_toText(dmp.patch_make(diff));
		}

		private Dictionary<int, Dictionary<int, List<List<KeyValuePair<int, int>>>>> FoundCombinations = new Dictionary<int, Dictionary<int, List<List<KeyValuePair<int, int>>>>>();

		private List<List<KeyValuePair<int, int>>> FindCombinations(List<List<KeyValuePair<int, object[]>>> outputValuesMatrix, List<KeyValuePair<int, int>> currentCombination)
		{
			var combinations = new List<List<KeyValuePair<int, int>>>();

			for (int i = 0; i < outputValuesMatrix.Count; i++)
			{
				if (!currentCombination.Any(c => c.Key == i))
				{
					for (int j = 0; j < outputValuesMatrix[i].Count; j++)
					{
						if (!currentCombination.Any(c => c.Value == j))
						{
							var newCurrentCombination = currentCombination.ToList();
							newCurrentCombination.Add(new KeyValuePair<int, int>(i, j));

							combinations.AddRange(FindCombinations(outputValuesMatrix, newCurrentCombination));
						}
					}
				}
			}

			if (combinations.Count == 0)
			{
				combinations.Add(currentCombination);
			}

			return combinations;
		}

		#endregion Run Time Methods
	}
}
