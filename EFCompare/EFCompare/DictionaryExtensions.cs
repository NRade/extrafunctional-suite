﻿using System.Collections.Generic;

namespace EFCompare
{
	public static class DictionaryExtensions
	{
		/// <summary>
		/// gets the value at the given index or adds a new entry at the index and returns it
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <param name="dictionary"></param>
		/// <param name="inputValue"></param>
		/// <returns></returns>
		public static T2 GetValueOrAddedNew<T1, T2>(this Dictionary<T1, T2> dictionary, T1 inputValue) where T2 : new()
		{
			if (!dictionary.TryGetValue(inputValue, out T2 outputValue))
			{
				outputValue = new T2();
				dictionary.Add(inputValue, outputValue);
			}
			return outputValue;
		}
	}
}
