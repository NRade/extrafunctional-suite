﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFJoin
{
    class SortableObjectList : IComparable<SortableObjectList>
    {
        public List<IComparable> objectList = new List<IComparable>();

        private List<bool> invertedList = new List<bool>();

        public void Add(IComparable obj, bool inverted)
        {
            objectList.Add(obj);
            invertedList.Add(inverted);
        }

        public int CompareTo(SortableObjectList obj)
        {
            for(int i = 0; i < objectList.Count; i++)
            {
                int comparisonValue = 0;

                if (objectList[i] is Guid)
                {
                    // guids are compared using the logic in the SqlGuid class, as they are compared in SQL Server
                    comparisonValue = (new SqlGuid((Guid)objectList[i])).CompareTo(new SqlGuid((Guid)obj.objectList[i]));
                }
                else
                {
                    comparisonValue = objectList[i].CompareTo(obj.objectList[i]);
                }
                
                if (comparisonValue != 0)
                {
                    if (invertedList[i]) comparisonValue = -comparisonValue;

                    return comparisonValue;
                }
            }
            return 0;
        }
    }
}
