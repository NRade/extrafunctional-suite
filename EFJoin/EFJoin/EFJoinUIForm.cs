﻿using EFJoin.ProcessTiming;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace EFJoin
{
	public partial class EFJoinUIForm : System.Windows.Forms.Form
	{
		private Connections connections;
		private Variables variables;
		private IDTSComponentMetaData100 metaData;

		private CManagedComponentWrapper designTimeInstance;

		private CManagedComponentWrapper DesignTimeInstance
		{
			get
			{
				designTimeInstance = designTimeInstance ?? metaData.Instantiate();
				return designTimeInstance;
			}
		}
		private Button button1;
		private Button button2;
		private DataGridView outputColumnsGrid;
		private TextBox textBox1;
		private DataGridView outerJoinGrid;
		private DataGridViewTextBoxColumn OuterJoin;
		private DataGridViewTextBoxColumn Datatype;
		private DataGridViewTextBoxColumn Origin;
		private DataGridViewTextBoxColumn InputColumn;
		private DataGridViewCheckBoxColumn Include;
		private DataGridViewTextBoxColumn SortKeyIn;
		private DataGridViewTextBoxColumn SortKeyOut;
		private DataGridViewTextBoxColumn OutputAlias;
		private DataGridViewTextBoxColumn OldInputName;
		private bool freezeCellValueChangedEvent = true;

		public EFJoinUIForm(Connections cons, Variables vars, IDTSComponentMetaData100 md)
		{
			ProcessTimeLog.Instance.StartProcess("Initialization");

			variables = vars;
			connections = cons;
			metaData = md;

			InitializeComponent();

			outerJoinGrid.Rows.Add();
			outerJoinGrid.Rows.Add();
			outerJoinGrid[0, 0].Value = "Input Name:";
			outerJoinGrid[0, 1].Value = "Outer Join:";
			outerJoinGrid.Rows[outerJoinGrid.Rows.Add()].Visible = false;


			for (int i = 0; i < metaData.InputCollection.Count; i++)
			{
				var input = metaData.InputCollection[i];

				if (input.IsAttached)
				{
					var outerJoinProperty = input.CustomPropertyCollection.GetCustomPropertyOrCreate(EFJoin.outerJoinProperty, false);

					var newOuterJoinColumn = new DataGridViewTextBoxColumn
					{
						AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
					};

					outerJoinGrid.Columns.Add(newOuterJoinColumn);

					outerJoinGrid[i + 1, 0].Value = input.Name;
					outerJoinGrid[i + 1, 1] = new DataGridViewCheckBoxCell
					{
						Value = outerJoinProperty.Value
					};
					outerJoinGrid[i + 1, 2].Value = input.Name;
				}

				var vInput = input.GetVirtualInput();

				foreach (IDTSVirtualInputColumn100 vInputColumn in vInput.VirtualInputColumnCollection)
				{
					var vInputFullName = EFJoin.GetUniqueVInputName(input.Name, vInputColumn.Name);

					ColumnParameters columnParameters;
					var property = metaData.GetCustomPropertyOrCreate(vInputFullName);
					if (property.Value == null)
					{
						columnParameters = new ColumnParameters
						{
							Included = true,
							OutputAlias = vInputColumn.Name,
							SortKeyOut = vInputColumn.SortKeyPosition
						};
						property.Value = columnParameters.Serialized();

					}
					else
					{
						columnParameters = ColumnParameters.DeSerialized((string[])property.Value);
					}

					StoredColumnParameters.Add(vInputFullName, columnParameters);

					var lastIndex = outputColumnsGrid.Rows.Add(new object[] {
						Convert.ToString(vInputColumn.DataType),
						input.Name,
						vInputColumn.Name,
						columnParameters.Included,
						Convert.ToString(vInputColumn.SortKeyPosition),
						Convert.ToString(columnParameters.SortKeyOut),
						columnParameters.OutputAlias,
						input.Name
					});

					if (i % 2 == 0)
					{
						outputColumnsGrid.Rows[lastIndex].DefaultCellStyle.BackColor = Color.FromArgb(235, 235, 235);
					}
					else
					{
						outputColumnsGrid.Rows[lastIndex].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 255);
					}
				}
			}

			outputColumnsGrid.CellMouseUp += DataGridView1_OnCellMouseUp;

			outputColumnsGrid.CellEndEdit += DataGridView1_CellEndEdit;

			outputColumnsGrid.CellValueChanged += DataGridView1_CellValueChanged;

			outputColumnsGrid.EditingControlShowing += OutputColumnsGrid_EditingControlShowing;

			outerJoinGrid.CellValueChanged += OuterJoinGrid_CellValueChanged;

			freezeCellValueChangedEvent = false;

			RefreshInfo();

			ProcessTimeLog.Instance.EndProcess("Initialization");
		}

		private List<string> outerJoinValidationMessages = new List<string>();

		private void OuterJoinGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			ProcessTimeLog.Instance.StartProcess("OuterJoinGrid CellValueChanged");

			if (e.RowIndex == 0)
			{
				foreach (DataGridViewRow row in outputColumnsGrid.Rows)
				{
					if (Convert.ToString(row.Cells[7].Value) == Convert.ToString(outerJoinGrid[e.ColumnIndex, 2].Value))
					{
						row.Cells[1].Value = outerJoinGrid[e.ColumnIndex, 0].Value;
					}
				}

				outerJoinValidationMessages = new List<string>();

				foreach (DataGridViewTextBoxCell cell in outerJoinGrid.Rows[0].Cells)
				{
					if (outerJoinGrid.Rows[0].Cells.Cast<DataGridViewTextBoxCell>().Any(c => c != cell && Convert.ToString(c.Value) == Convert.ToString(cell.Value)))
					{
						outerJoinValidationMessages.Add("Duplicate input name '" + Convert.ToString(cell.Value) + "'.");
					}
				}

				SetInformationLabel();
			}

			ProcessTimeLog.Instance.EndProcess("OuterJoinGrid CellValueChanged");
		}

		private void OutputColumnsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (outputColumnsGrid.CurrentCell.ColumnIndex == 5)
			{
				((TextBox)e.Control).KeyPress += EFJoinUIForm_KeyPress;
			}
		}

		private void EFJoinUIForm_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (outputColumnsGrid.CurrentCell.ColumnIndex == 5)
			{
				var textBox = (TextBox)sender;

				if (!(
					(e.KeyChar >= '0' && e.KeyChar <= '9' && !(textBox.Text.Contains('-') && textBox.Text.IndexOf('-') >= (textBox.SelectionStart + textBox.SelectionLength)))
					|| (e.KeyChar == '-' && textBox.SelectionStart == 0 && !textBox.Text.Contains('-'))
					|| e.KeyChar == '\b'))
				{
					e.Handled = true;
				}
			}
		}

		private void DataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (!freezeCellValueChangedEvent && e.ColumnIndex == 3 && e.RowIndex != -1)
			{
				RefreshInfo();
				HandleSameSortKeys(e.RowIndex, 6);
				RefreshInfo();
			}
		}

		private void HandleSameSortKeys(int rowIndex, int columnIndex)
		{
			ProcessTimeLog.Instance.StartProcess("Handle SortKeys");

			freezeCellValueChangedEvent = true;

			var selectedRow = outputColumnsGrid.Rows[rowIndex];

			var sortKeyOut = Convert.ToInt32(selectedRow.Cells[5].Value);
			if ((columnIndex == 6 || columnIndex == 5) && sortKeyOut != 0)
			{
				var dataType = (string)selectedRow.Cells[0].Value;
				var outputAlias = (string)selectedRow.Cells[6].Value;
				foreach (DataGridViewRow row in outputColumnsGrid.Rows)
				{
					if (Convert.ToInt32(row.Cells[5].Value) == sortKeyOut)
					{
						row.Cells[6].Value = outputAlias;
					}
				}
			}

			freezeCellValueChangedEvent = false;

			ProcessTimeLog.Instance.EndProcess("Handle SortKeys");
		}

		private List<int> sortKeysWithDatatypeMismatch = new List<int>();
		private Dictionary<int, string> sortKeyDataTypes = new Dictionary<int, string>();

		private void DataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			HandleSameSortKeys(e.RowIndex, e.ColumnIndex);

			RefreshInfo();
		}

		private void DataGridView1_OnCellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.ColumnIndex == 3 && e.RowIndex != -1)
			{
				outputColumnsGrid.EndEdit();
			}
		}

		private List<string> validationMessages = new List<string>();

		private bool sortKeysChanged = false;

		private Dictionary<string, ColumnParameters> StoredColumnParameters = new Dictionary<string, ColumnParameters>();

		private void RefreshInfo()
		{
			ProcessTimeLog.Instance.StartProcess("Refresh info");

			freezeCellValueChangedEvent = true;

			validationMessages = new List<string>();

			var sortKeyDict = new Dictionary<string, List<int>>();
			var inputsWithDuplicateSortKeys = new List<string>();
			var existingNames = new List<string>();
			sortKeysWithDatatypeMismatch = new List<int>();
			sortKeyDataTypes = new Dictionary<int, string>();

			sortKeysChanged = false;
			for (int i = 0; i < outputColumnsGrid.Rows.Count; i++)
			{
				var row = outputColumnsGrid.Rows[i];

				if (!(bool)row.Cells[3].Value)
				{
					row.Cells[5].ReadOnly = true;
					row.Cells[5].Value = null;
					row.Cells[6].ReadOnly = true;
					row.Cells[6].Value = null;
				}
				else
				{
					var columnParameters = StoredColumnParameters[EFJoin.GetUniqueVInputName(Convert.ToString(row.Cells[7].Value), (string)row.Cells[2].Value)];

					row.Cells[5].ReadOnly = false;
					if (row.Cells[5].Value == null)
					{
						row.Cells[5].Value = columnParameters.SortKeyOut;
					}
					row.Cells[6].ReadOnly = false;
					if (row.Cells[6].Value == null)
					{
						row.Cells[6].Value = columnParameters.OutputAlias;
					}

					var outputAlias = (string)row.Cells[6].Value;
					var sortKeyOut = Convert.ToInt32(row.Cells[5].Value);

					if (string.IsNullOrWhiteSpace(outputAlias))
					{
						validationMessages.Add("Output names of included columns can not be empty.");
					}
					else
					{
						if (sortKeyOut == 0)
						{
							if (existingNames.Contains(outputAlias))
							{
								validationMessages.Add("Output names must be unique for columns that don't share a sort key.");
							}
							else
							{
								existingNames.Add(outputAlias);
							}
						}
					}

					if (Convert.ToInt32(row.Cells[4].Value) != sortKeyOut)
					{
						sortKeysChanged = true;
					}

					var inputSortKeysList = sortKeyDict.GetValueOrAddedNew((string)row.Cells[1].Value);
					if (inputSortKeysList.Contains(sortKeyOut))
					{
						inputsWithDuplicateSortKeys.Add((string)row.Cells[1].Value);
					}
					else
					{
						if (row.Cells[5].Value != null && sortKeyOut != 0)
						{
							inputSortKeysList.Add(sortKeyOut);
						}
					}

					var dataType = (string)row.Cells[0].Value;

					if (sortKeyOut != 0)
					{
						if (sortKeyDataTypes.TryGetValue(sortKeyOut, out string existingDataType))
						{
							if (existingDataType != dataType)
							{
								sortKeysWithDatatypeMismatch.Add(sortKeyOut);
							}
						}
						else
						{
							sortKeyDataTypes.Add(sortKeyOut, dataType);
						}
					}
				}
			}

			if (sortKeysWithDatatypeMismatch.Any())
			{
				validationMessages.Add("Mismatching datatypes for sort keys: " + String.Join(", ", sortKeysWithDatatypeMismatch));
			}

			if (inputsWithDuplicateSortKeys.Any())
			{
				validationMessages.Add("Duplicate output sort keys for inputs: " + String.Join(", ", inputsWithDuplicateSortKeys));
			}
			else
			{
				List<int> comparisonList = null;

				foreach (var entry in sortKeyDict)
				{
					if (entry.Value.Count == 0)
					{
						validationMessages.Add("All inputs should have at least one output sort key other than 0.");
					}

					if (comparisonList == null)
					{
						comparisonList = entry.Value;
					}
					else
					{
						if (entry.Value.Count != comparisonList.Count || comparisonList.Any(i => !entry.Value.Contains(i)))
						{
							validationMessages.Add("All inputs should have the same output sort key numbers.");
						}
					}
				}
			}

			SetInformationLabel();

			freezeCellValueChangedEvent = false;

			ProcessTimeLog.Instance.EndProcess("Refresh info");
		}

		private void SetInformationLabel()
		{
			var concatMessages = validationMessages.Concat(outerJoinValidationMessages);

			if (concatMessages.Any())
			{
				textBox1.Text = "Invalid input: " + concatMessages.Last();
			}
			else
			{
				if (sortKeysChanged)
				{
					textBox1.Text = "Warning: sort keys differ between input and output. Performance will be compromised.";
				}
				else
				{
					textBox1.Text = "Output columns with matching sort keys will be merged. Rows from different inputs will be joined where sorted values match.";
				}
			}
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EFJoinUIForm));
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.outputColumnsGrid = new System.Windows.Forms.DataGridView();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.outerJoinGrid = new System.Windows.Forms.DataGridView();
			this.OuterJoin = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Datatype = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Origin = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.InputColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Include = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.SortKeyIn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SortKeyOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.OutputAlias = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.OldInputName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.outputColumnsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.outerJoinGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(752, 530);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(671, 530);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 3;
			this.button2.Text = "cancel";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// outputColumnsGrid
			// 
			this.outputColumnsGrid.AllowUserToAddRows = false;
			this.outputColumnsGrid.AllowUserToDeleteRows = false;
			this.outputColumnsGrid.AllowUserToResizeRows = false;
			this.outputColumnsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.outputColumnsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.outputColumnsGrid.BackgroundColor = System.Drawing.SystemColors.ControlLight;
			this.outputColumnsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.outputColumnsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Datatype,
			this.Origin,
			this.InputColumn,
			this.Include,
			this.SortKeyIn,
			this.SortKeyOut,
			this.OutputAlias,
			this.OldInputName});
			this.outputColumnsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.outputColumnsGrid.Location = new System.Drawing.Point(15, 62);
			this.outputColumnsGrid.MultiSelect = false;
			this.outputColumnsGrid.Name = "outputColumnsGrid";
			this.outputColumnsGrid.RowHeadersVisible = false;
			this.outputColumnsGrid.Size = new System.Drawing.Size(812, 462);
			this.outputColumnsGrid.TabIndex = 4;
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(12, 533);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(653, 20);
			this.textBox1.TabIndex = 5;
			// 
			// outerJoinGrid
			// 
			this.outerJoinGrid.AllowUserToAddRows = false;
			this.outerJoinGrid.AllowUserToDeleteRows = false;
			this.outerJoinGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.outerJoinGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.outerJoinGrid.ColumnHeadersVisible = false;
			this.outerJoinGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.OuterJoin});
			this.outerJoinGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.outerJoinGrid.Location = new System.Drawing.Point(15, 10);
			this.outerJoinGrid.MultiSelect = false;
			this.outerJoinGrid.Name = "outerJoinGrid";
			this.outerJoinGrid.RowHeadersVisible = false;
			this.outerJoinGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.outerJoinGrid.Size = new System.Drawing.Size(812, 47);
			this.outerJoinGrid.TabIndex = 6;
			// 
			// OuterJoin
			// 
			this.OuterJoin.HeaderText = "";
			this.OuterJoin.Name = "OuterJoin";
			this.OuterJoin.ReadOnly = true;
			// 
			// Datatype
			// 
			this.Datatype.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.Datatype.HeaderText = "Datatype";
			this.Datatype.Name = "Datatype";
			this.Datatype.ReadOnly = true;
			this.Datatype.Width = 75;
			// 
			// Origin
			// 
			this.Origin.HeaderText = "Origin";
			this.Origin.Name = "Origin";
			this.Origin.ReadOnly = true;
			this.Origin.Width = 59;
			// 
			// InputColumn
			// 
			this.InputColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
			this.InputColumn.HeaderText = "Input Column";
			this.InputColumn.Name = "InputColumn";
			this.InputColumn.ReadOnly = true;
			this.InputColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.InputColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.InputColumn.Width = 75;
			// 
			// Include
			// 
			this.Include.HeaderText = "Include";
			this.Include.Name = "Include";
			this.Include.Width = 48;
			// 
			// SortKeyIn
			// 
			this.SortKeyIn.HeaderText = "SortKey In";
			this.SortKeyIn.Name = "SortKeyIn";
			this.SortKeyIn.ReadOnly = true;
			this.SortKeyIn.Width = 81;
			// 
			// SortKeyOut
			// 
			this.SortKeyOut.HeaderText = "SortKeyOut";
			this.SortKeyOut.Name = "SortKeyOut";
			this.SortKeyOut.Width = 86;
			// 
			// OutputAlias
			// 
			this.OutputAlias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.OutputAlias.HeaderText = "Output Alias";
			this.OutputAlias.Name = "OutputAlias";
			// 
			// OldInputName
			// 
			this.OldInputName.HeaderText = "OldName";
			this.OldInputName.Name = "OldInputName";
			this.OldInputName.Visible = false;
			this.OldInputName.Width = 76;
			// 
			// EFJoinUIForm
			// 
			this.ClientSize = new System.Drawing.Size(839, 565);
			this.Controls.Add(this.outerJoinGrid);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.outputColumnsGrid);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EFJoinUIForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "EFJoin Editor";
			((System.ComponentModel.ISupportInitialize)(this.outputColumnsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.outerJoinGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (!validationMessages.Any())
			{
				foreach (DataGridViewRow row in outputColumnsGrid.Rows)
				{
					var property = metaData.CustomPropertyCollection.Cast<IDTSCustomProperty100>().First(cp => cp.Name == EFJoin.GetUniqueVInputName(Convert.ToString(row.Cells[7].Value), (string)row.Cells[2].Value));

					var columnParameters = ColumnParameters.DeSerialized((string[])property.Value);
					columnParameters.Included = Convert.ToBoolean(row.Cells[3].Value);
					if (row.Cells[5].Value != null)
					{
						columnParameters.SortKeyOut = Convert.ToInt32(row.Cells[5].Value);
					}
					if (row.Cells[6].Value != null)
					{
						columnParameters.OutputAlias = (string)row.Cells[6].Value;
					}

					metaData.CustomPropertyCollection.RemoveObjectByID(property.ID);

					var newProperty = metaData.CustomPropertyCollection.New();
					newProperty.Name = EFJoin.GetUniqueVInputName(Convert.ToString(row.Cells[1].Value), (string)row.Cells[2].Value);
					newProperty.Value = columnParameters.Serialized();
				}

				for (int i = 1; i < outerJoinGrid.ColumnCount; i++)
				{
					var outerJoinProperty = metaData.InputCollection[i - 1].CustomPropertyCollection.GetCustomPropertyOrCreate(EFJoin.outerJoinProperty);

					outerJoinProperty.Value = Convert.ToBoolean(outerJoinGrid[i, 1].Value);

					metaData.InputCollection[i - 1].Name = Convert.ToString(outerJoinGrid[i, 0].Value);
				}

				this.DialogResult = DialogResult.OK;

				this.Close();
			}
			else
			{
				MessageBox.Show(String.Join(Environment.NewLine, validationMessages.Distinct()), "Invalid input");
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			var avgDurations = ProcessTimeLog.Instance.ReportAverageDurations();

			this.DialogResult = DialogResult.Cancel;

			this.Close();
		}
	}
}
