﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFJoin
{
    class InputCurrentJoinValue
    {
        private InputState state = InputState.Unstarted;

        public InputState State
        {
            get
            {
                return state;
            }
        }

        private object joinValue;

        public object JoinValue
        {
            set
            {
                state = InputState.Started;
                joinValue = value;
            }
            private get
            {
                return joinValue;
            }
        }

        public bool InputAheadOf(object otherJoinValue)
        {
            switch(State)
            {
                case InputState.Unstarted:
                    return false;
                case InputState.Finished:
                    return true;
                default:
                    return Comparer<SortableObjectList>.Default.Compare((SortableObjectList)joinValue, (SortableObjectList)otherJoinValue) > 0;
            }
        }

        public void SetFinished()
        {
            state = InputState.Finished;
        }
    }

    enum InputState
    {
        Unstarted,
        Started,
        Finished
    }
}
