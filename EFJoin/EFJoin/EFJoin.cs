﻿using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFJoin
{
	[DtsPipelineComponent(DisplayName = "EFJoin",
		Description = "Joins multiple dataflows into one output using the SQL Join principle, merging the columns that are joined on and preserving the sorting hierarchy.",
		UITypeName = "EFJoin.EFJoinComponentUI,EFJoin,Version=1.0.0.0,Culture=neutral,PublicKeyToken=ff18449c97125fe8",
		ComponentType = ComponentType.Transform,
		IconResource = "EFJoin.EFJoinIconLarge.ico")]
	public class EFJoin : PipelineComponent
	{
		private int[][] inputBufferColumnIndexes;
		private int[] outputBufferColumnIndexes;

		CurrentValuesStructure CurrentValueStructure;

		List<KeyValuePair<bool, int>>[] joinIndexesByInputIndex;

		private bool sortKeyPositionsChanged = false;

		private PipelineBuffer outputBuffer;

		private IDTSOutput100 output;

		private const string inputColumnsValidProperty = "areInputColumnsValid";

		public const string outerJoinProperty = "outerJoin";

		#region Design Time Methods

		//Design time - constructor
		public override void ProvideComponentProperties()
		{
			// Set component information
			ComponentMetaData.Name = "EFJoin";
			ComponentMetaData.Description = "Joins multiple dataflows into one output using the SQL Join principle, merging the column that is joined on and preserving the sorting hierarchy.";
			ComponentMetaData.ContactInfo = "N.C. Rademaker";

			base.RemoveAllInputsOutputsAndCustomProperties();

			IDTSInput100 input = ComponentMetaData.InputCollection.New();
			input.Name = "0";

			IDTSOutput100 output = ComponentMetaData.OutputCollection.New();
			output.Name = "Output";
			output.SynchronousInputID = 0;
			output.IsSorted = true;

		}

		//Design time - Metadata Validator
		public override DTSValidationStatus Validate()
		{
			if (ComponentMetaData.OutputCollection.Count != 1)
			{
				ComponentMetaData.FireError(0, ComponentMetaData.Name, "Incorrect number of outputs.", "", 0, out bool pbCancel);
				return DTSValidationStatus.VS_ISCORRUPT;
			}

			var outputColumns = ComponentMetaData.OutputCollection[0].OutputColumnCollection;

			var validOutputColumnIds = new List<int>();

			foreach (IDTSInput100 input in ComponentMetaData.InputCollection)
			{
				var virtualInput = input.GetVirtualInput();

				var areInputColumnsValid = input.CustomPropertyCollection.GetCustomPropertyOrCreate(inputColumnsValidProperty, true);

				foreach (IDTSInputColumn100 inputColumn in input.InputColumnCollection)
				{
					IDTSVirtualInputColumn100 vColumn;
					try
					{
						vColumn = virtualInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(inputColumn.LineageID);
					}
					catch
					{
						ComponentMetaData.FireError(0, ComponentMetaData.Name, "The input column " + inputColumn.IdentificationString + " does not match a column in the upstream component.", "", 0, out bool cancel);
						areInputColumnsValid.Value = false;
						return DTSValidationStatus.VS_NEEDSNEWMETADATA;
					}


					var propertyValue = ComponentMetaData.GetCustomPropertyValue<string[]>(GetUniqueVInputName(input.Name, vColumn.Name));
					ColumnParameters columnParameters = ColumnParameters.DeSerialized(propertyValue);

					IDTSOutputColumn100 outputColumn = null;
					if (inputColumn.MappedColumnID >= 0)
					{
						try
						{
							outputColumn = outputColumns.FindObjectByID(inputColumn.MappedColumnID);
						}
						catch
						{

						}
					}

					if (outputColumn == null)
					{
						if (columnParameters.Included)
						{
							if (columnParameters.SortKeyOut != 0)
							{
								outputColumn = outputColumns.Cast<IDTSOutputColumn100>().FirstOrDefault(c => c.SortKeyPosition == columnParameters.SortKeyOut);

								if (outputColumn == null)
								{
									outputColumn = outputColumns.New();
								}
							}
							else
							{
								outputColumn = outputColumns.New();
							}
						}
					}
					else
					{
						if (!columnParameters.Included)
						{
							outputColumns.RemoveObjectByID(outputColumn.ID);
							outputColumn = null;
						}
					}

					if (outputColumn != null)
					{
						outputColumn.Name = columnParameters.OutputAlias;
						outputColumn.SortKeyPosition = columnParameters.SortKeyOut;
						inputColumn.MappedColumnID = outputColumn.ID;
						outputColumn.SetDataTypeProperties(inputColumn.DataType, inputColumn.Length, inputColumn.Precision, inputColumn.Scale, inputColumn.CodePage);
						validOutputColumnIds.Add(outputColumn.ID);
					}
					else
					{
						inputColumn.MappedColumnID = -1;
					}
				}
			}

			var invalidOutputColumnIds = new List<int>();
			foreach (IDTSOutputColumn100 outputColumn in outputColumns)
			{
				if (!validOutputColumnIds.Contains(outputColumn.ID))
				{
					invalidOutputColumnIds.Add(outputColumn.ID);
				}
			}

			foreach (var invalidId in invalidOutputColumnIds)
			{
				outputColumns.RemoveObjectByID(invalidId);
			}

			return DTSValidationStatus.VS_ISVALID;
		}

		//Design Time - method to autocorrect VS_NEEDSNEWMETADATA error
		public override void ReinitializeMetaData()
		{
			foreach (IDTSInput100 input in ComponentMetaData.InputCollection)
			{
				var areInputColumnsValid = input.CustomPropertyCollection.GetCustomPropertyOrCreate(inputColumnsValidProperty, false);
				if (!(bool)areInputColumnsValid.Value)
				{
					IDTSVirtualInput100 vInput = input.GetVirtualInput();

					foreach (IDTSInputColumn100 column in input.InputColumnCollection)
					{
						IDTSVirtualInputColumn100 vColumn = vInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(column.LineageID);

						if (vColumn == null)
							input.InputColumnCollection.RemoveObjectByID(column.ID);
					}
					areInputColumnsValid.Value = true;
				}
			}
		}

		public static string GetUniqueVInputName(string origin, string name)
		{
			return origin.Replace(' ', '_') + "-" + name;
		}

		public override void OnInputPathAttached(int inputID)
		{
			var input = ComponentMetaData.InputCollection.GetObjectByID(inputID);
			var vInput = input.GetVirtualInput();

			var existingOutputNames = new List<KeyValuePair<int, string>>();
			foreach (IDTSOutput100 output in ComponentMetaData.OutputCollection)
			{
				foreach (IDTSOutputColumn100 outputColumn in output.OutputColumnCollection)
				{
					existingOutputNames.Add(new KeyValuePair<int, string>(outputColumn.SortKeyPosition, outputColumn.Name));
				}
			}

			for (int i = 0; i < vInput.VirtualInputColumnCollection.Count; i++)
			{
				var vColumn = vInput.VirtualInputColumnCollection[i];

				var property = ComponentMetaData.GetCustomPropertyOrCreate(GetUniqueVInputName(input.Name, vColumn.Name));

				var outputAlias = vColumn.Name;

				bool outputAliasExists = true;

				while (outputAliasExists)
				{
					outputAliasExists = false;
					foreach (var outputNameKvp in existingOutputNames)
					{
						if (outputAlias == outputNameKvp.Value && outputNameKvp.Key != vColumn.SortKeyPosition)
						{
							outputAlias += "'";
							outputAliasExists = true;
						}
					}
				}

				existingOutputNames.Add(new KeyValuePair<int, string>(vColumn.SortKeyPosition, outputAlias));

				property.Value = new ColumnParameters
				{
					Included = true,
					SortKeyOut = vColumn.SortKeyPosition,
					OutputAlias = outputAlias
				}.Serialized();

				if (!input.InputColumnCollection.Cast<IDTSInputColumn100>().Any(ic => ic.LineageID == vColumn.LineageID))
				{
					var newColumn = input.InputColumnCollection.New();
					newColumn.LineageID = vColumn.LineageID;
					newColumn.Name = vColumn.Name;
				}
			}

			// create new empty input that the next path can be attached to
			var newInput = ComponentMetaData.InputCollection.New();
			var newIndex = 0;

			while (ComponentMetaData.InputCollection.Cast<IDTSInput100>().Any(o => o.Name == "" + newIndex))
			{
				newIndex++;
			}
			newInput.Name = "" + newIndex;
		}

		public override void OnInputPathDetached(int inputID)
		{
			var input = ComponentMetaData.InputCollection.GetObjectByID(inputID);

			for (int i = 0; i < input.InputColumnCollection.Count; i++)
			{
				var outputColumns = ComponentMetaData.OutputCollection[0].OutputColumnCollection;

				var mappedColumnId = input.InputColumnCollection[i].MappedColumnID;

				if (mappedColumnId >= 0)
				{
					var mappedOutputColumn = outputColumns.GetObjectByID(mappedColumnId);
					// remove the output column that is joined on only if this is the last input to be deleted (excluding the empty last input)
					if (mappedOutputColumn.SortKeyPosition == 0 || ComponentMetaData.InputCollection.Count == 2)
					{
						outputColumns.RemoveObjectByID(mappedOutputColumn.ID);
					}
				}
			}

			ComponentMetaData.InputCollection.RemoveObjectByID(inputID);
		}

		//Override InsertOutputColumnAt to prevent addition of new column from Advanced Editor
		public override IDTSOutputColumn100 InsertOutputColumnAt(
			 int outputID,
			 int outputColumnIndex,
			 string name,
			 string description)
		{
			ComponentMetaData.FireError(0, ComponentMetaData.Name, "Output columns cannot be added to " + ComponentMetaData.Name, "", 0, out bool cancel);
			//bubble-up the error to VS
			throw new Exception("Output columns cannot be added to " + ComponentMetaData.Name, null);
		}

		#endregion Design Time Methods

		#region Run Time Methods

		//Run Time - Pre Execute identifying the input columns in this component from Buffer Manager
		public override void PreExecute()
		{
			sortKeyPositionsChanged = false;

			output = ComponentMetaData.OutputCollection[0];

			joinIndexesByInputIndex = new List<KeyValuePair<bool, int>>[ComponentMetaData.InputCollection.Count - 1];

			CurrentValueStructure = new CurrentValuesStructure(ComponentMetaData.InputCollection.Count - 1);

			inputBufferColumnIndexes = new int[ComponentMetaData.InputCollection.Count - 1][];
			for (int h = 0; h < ComponentMetaData.InputCollection.Count - 1; h++)
			{
				var input = ComponentMetaData.InputCollection[h];

				var joinIndexesTemp = new SortedList<int, KeyValuePair<bool, int>>();

				inputBufferColumnIndexes[h] = new int[input.InputColumnCollection.Count];
				for (int i = 0; i < input.InputColumnCollection.Count; i++)
				{
					inputBufferColumnIndexes[h][i] = BufferManager.FindColumnByLineageID(input.Buffer, input.InputColumnCollection[i].LineageID);

					var mappedColumnId = input.InputColumnCollection[i].MappedColumnID;
					if (mappedColumnId >= 0)
					{
						var mappedColumn = output.OutputColumnCollection.FindObjectByID(mappedColumnId);

						if (mappedColumn.SortKeyPosition != 0)
						{
							joinIndexesTemp.Add(Math.Abs(mappedColumn.SortKeyPosition), new KeyValuePair<bool, int>(mappedColumn.SortKeyPosition < 0, i));
						}

						if (input.InputColumnCollection[i].SortKeyPosition != mappedColumn.SortKeyPosition)
						{
							sortKeyPositionsChanged = true;
						}
					}
				}

				joinIndexesByInputIndex[h] = joinIndexesTemp.Values.ToList();
			}

			outputBufferColumnIndexes = new int[output.OutputColumnCollection.Count];
			for (int i = 0; i < output.OutputColumnCollection.Count; i++)
			{
				outputBufferColumnIndexes[i] = BufferManager.FindColumnByLineageID(output.Buffer, output.OutputColumnCollection[i].LineageID);
			}
		}

		public override void PrimeOutput(int outputs, int[] outputIDs, PipelineBuffer[] buffers)
		{
			if (buffers.Any())
			{
				outputBuffer = buffers[0];
			}
		}

		public override void ProcessInput(int inputID, PipelineBuffer buffer)
		{
			if (!buffer.EndOfRowset)
			{
				while (buffer.NextRow())
				{
					var inputIndex = ComponentMetaData.InputCollection.FindObjectIndexByID(inputID);
					var input = ComponentMetaData.InputCollection[inputIndex];

					var joinValue = new SortableObjectList();
					foreach (var kvp in joinIndexesByInputIndex[inputIndex])
					{
						joinValue.Add((IComparable)buffer[inputBufferColumnIndexes[inputIndex][kvp.Value]], kvp.Key);
					}

					var values = new object[input.InputColumnCollection.Count];
					for (int inputColumnIndex = 0; inputColumnIndex < input.InputColumnCollection.Count; inputColumnIndex++)
					{
						values[inputColumnIndex] = buffer[inputBufferColumnIndexes[inputIndex][inputColumnIndex]];
					}

					CurrentValueStructure.AddCurrentValues(joinValue, inputIndex, values);

					if (!sortKeyPositionsChanged)
					{
						if (CurrentValueStructure.ExtractFirstInLineValuesIfReady(out List<object[]>[] valuesByInputIndex))
						{
							OutputJoinedLists(valuesByInputIndex, 0, new object[output.OutputColumnCollection.Count]);
							ifCounter++;
						}
					}
				}
			}
			else
			{
				var inputIndex = ComponentMetaData.InputCollection.FindObjectIndexByID(inputID);

				CurrentValueStructure.SetInputFinished(inputIndex);

				while (CurrentValueStructure.HasJoinValuePages() && CurrentValueStructure.ExtractFirstInLineValuesIfReady(out List<object[]>[] valuesByInputIndex))
				{
					OutputJoinedLists(valuesByInputIndex, 0, new object[output.OutputColumnCollection.Count]);
					whileCounter++;
				}
				whileOutCounter++;

				if (CurrentValueStructure.AllInputsFinished())
				{
					outputBuffer.SetEndOfRowset();
				}
			}
		}

		private int whileOutCounter = 0;
		private int whileCounter = 0;
		private int ifCounter = 0;

		private void OutputJoinedLists(List<object[]>[] inputIndexPages, int inputIndex, object[] outputRowValues)
		{
			var valuesLists = inputIndexPages[inputIndex];

			if (!valuesLists.Any())
			{
				if (ComponentMetaData.InputCollection[inputIndex].CustomPropertyCollection.GetCustomPropertyValue<bool>(outerJoinProperty))
				{
					valuesLists.Add(new object[ComponentMetaData.InputCollection[inputIndex].InputColumnCollection.Count]);
				}
			}

			foreach (var valuesList in valuesLists)
			{
				var currentOutputRowValues = outputRowValues.ToArray();

				var input = ComponentMetaData.InputCollection[inputIndex];
				for (int j = 0; j < input.InputColumnCollection.Count; j++)
				{
					var mappedColumnId = input.InputColumnCollection[j].MappedColumnID;
					if (mappedColumnId >= 0)
					{
						var mappedOutputColumnIndex = output.OutputColumnCollection.GetObjectIndexByID(mappedColumnId);

						currentOutputRowValues[mappedOutputColumnIndex] = currentOutputRowValues[mappedOutputColumnIndex] ?? valuesList[j];
					}
				}

				if (inputIndex + 1 < inputIndexPages.Length)
				{
					OutputJoinedLists(inputIndexPages, inputIndex + 1, currentOutputRowValues); // copies array
				}
				else
				{
					outputBuffer.AddRow();

					for (int k = 0; k < currentOutputRowValues.Length; k++)
					{
						var outputValue = currentOutputRowValues[k];

						var blobColumn = outputValue as BlobColumn;
						if (blobColumn != null)
						{
							if (blobColumn.Length > 0)
							{
								outputValue = blobColumn.GetBlobData(0, Convert.ToInt32(blobColumn.Length));
							}
							else
							{
								outputValue = null;
							}
						}

						if (outputValue != null)
						{
							outputBuffer[outputBufferColumnIndexes[k]] = outputValue;
						}
						else
						{
							outputBuffer.SetNull(outputBufferColumnIndexes[k]);
						}
					}
				}
			}
		}

		#endregion Run Time Methods
	}
}
