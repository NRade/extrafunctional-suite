﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EFJoin
{
    class ColumnParameters
    {
        public bool Included;

        public int SortKeyOut;

        public string OutputAlias;
        
        public string[] Serialized()
        {
            return typeof(ColumnParameters).GetFields(BindingFlags.Instance | BindingFlags.Public).Select(p =>
            {
                var value = p.GetValue(this);
                if (value != null) return value.ToString();
                return null;
            }).ToArray();
        }

        public static ColumnParameters DeSerialized(string[] xmlText)
        {
            return new ColumnParameters {
                Included = Convert.ToBoolean(xmlText[0]),
                SortKeyOut = Convert.ToInt32(xmlText[1]),
                OutputAlias = xmlText[2]
            };
        }
    }
}
