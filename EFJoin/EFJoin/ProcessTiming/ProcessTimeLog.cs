﻿using System.Collections.Generic;
using System.Linq;

namespace EFJoin.ProcessTiming
{
	/// <summary>
	/// process time log for profiling and performance analysis
	/// </summary>
	public class ProcessTimeLog
	{
		Dictionary<string, ProcessTimeEntry> processStructure = new Dictionary<string, ProcessTimeEntry>();

		/// <summary>
		/// singleton instance
		/// </summary>
		public static ProcessTimeLog Instance = new ProcessTimeLog();

		/// <summary>
		/// logged messages
		/// </summary>
		public string LoggedMessages { get; private set; }

		/// <summary>
		/// mark the start of the process by the given name
		/// </summary>
		/// <param name="processName"></param>
		/// <returns></returns>
		public string StartProcess(string processName)
		{
			if (!processStructure.TryGetValue(processName, out ProcessTimeEntry entry))
			{
				entry = new ProcessTimeEntry(processName);
				processStructure.Add(processName, entry);
			}

			return entry.StartProcess();
		}

		/// <summary>
		/// mark the end of the process by the given name
		/// </summary>
		/// <param name="processName"></param>
		/// <returns></returns>
		public string EndProcess(string processName)
		{
			if (!processStructure.TryGetValue(processName, out ProcessTimeEntry entry))
			{
				return "Process [" + processName + "] was never started.";
			}

			return entry.EndProcess();
		}

		/// <summary>
		/// mark the start of the processes by the given names and log resulting messages
		/// </summary>
		/// <param name="processNames"></param>
		public void LogStartProcess(params string[] processNames)
		{
			foreach (var processName in processNames)
			{
				LoggedMessages += StartProcess(processName) + "\n";
			}
		}

		/// <summary>
		/// mark the end of the processes by the given names and log resulting messages
		/// </summary>
		/// <param name="processNames"></param>
		public void LogEndProcess(params string[] processNames)
		{
			foreach (var processName in processNames)
			{
				LoggedMessages += EndProcess(processName) + "\n";
			}
		}

		/// <summary>
		/// report average durations of all recorded processes
		/// </summary>
		/// <returns></returns>
		public string ReportAverageDurations()
		{
			return string.Join("\n", processStructure.Values.Select(v => v.ReportAverageDuration()));
		}
	}
}
