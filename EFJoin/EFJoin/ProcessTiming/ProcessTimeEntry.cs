﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFJoin.ProcessTiming
{
    public class ProcessTimeEntry
    {
        /// <summary>
        /// constructor with given process name
        /// </summary>
        /// <param name="processName"></param>
        public ProcessTimeEntry(string processName)
        {
            ProcessName = processName;
            NumberOfExecutions = 0;
        }

        /// <summary>
        /// name of the process
        /// </summary>
        public string ProcessName { get; private set; }

        /// <summary>
        /// last start time
        /// </summary>
        public DateTime LastStartTime { get; private set; }

        /// <summary>
        /// average duration
        /// </summary>
        public TimeSpan AverageDuration { get; private set; }

        /// <summary>
        /// number of executions
        /// </summary>
        public int NumberOfExecutions { get; private set; }

        /// <summary>
        /// last duration
        /// </summary>
        public TimeSpan LastDuration { get; private set; }

        /// <summary>
        /// mark the start of the process
        /// </summary>
        /// <returns></returns>
        public string StartProcess()
        {
            LastStartTime = DateTime.Now;

            return "Starting process [" + ProcessName + "] at [" + DisplayDateTime(LastStartTime) + "]";
        }

        /// <summary>
        /// mark the end of the process
        /// </summary>
        /// <returns></returns>
        public string EndProcess()
        {
            var _endTime = DateTime.Now;

            var LastDuration = _endTime - LastStartTime;

            if (NumberOfExecutions == 0)
            {
                AverageDuration = new TimeSpan(LastDuration.Ticks);
            }
            else
            {
                AverageDuration = new TimeSpan(((AverageDuration.Ticks * NumberOfExecutions) + LastDuration.Ticks) / (NumberOfExecutions + 1));
            }
            NumberOfExecutions++;

            return "Ending process [" + ProcessName + "] at [" + DisplayDateTime(_endTime) + "]; Duration: [" + LastDuration + "]";
        }

        /// <summary>
        /// report average duration of this process in text
        /// </summary>
        /// <returns></returns>
        public string ReportAverageDuration()
        {
            return "Process [" + ProcessName + "] was executed [" + NumberOfExecutions + "] times, with an average duration of [" + AverageDuration + "] and total duration of [" + new TimeSpan(NumberOfExecutions * AverageDuration.Ticks) + "]";
        }

        /// <summary>
        /// display date time
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private string DisplayDateTime(DateTime dateTime)
        {
            return Convert.ToString(dateTime.Date).Split(' ')[0] + " " + dateTime.TimeOfDay;
        }
    }
}
