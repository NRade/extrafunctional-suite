# ExtraFunctional SSIS Data Flow Suite Documentation

# EFJoin

The EFJoin component combines columns from multiple dataflows based on the SQL Join principle. Unlike the original Merge Join component, there is no limit to how many inputs can be joined together at once. Also, it is not a strict requirement to provide the input dataflows sorted on the columns that they are joined on. For each input, it can be specified whether it is joined based on the Outer Join principle or not. For Outer Joined inputs it is not required for an input row to exist for a corresponding row to appear in the output.

![alt text](https://i.imgur.com/J9KwazL.png)

![alt text](https://i.imgur.com/Qq6V4He.png)

EFJoin works more efficiently when inputs are provided sorted on their joined columns, even though it is not required. The component provides a warning about this efficiency malus when the user does not provide the input sorted. However, the component will still sort the inputs more efficiently than using separate Sort components on each input upstream would.

EFJoin determines which columns should be used to join the dataflows on by examining the provided output sort keys. Where these match for different inputs, the inputs will be joined on the columns in question and the columns from each input merged into one output column. In the example above, subjectId is found in all inputs and given a SortKeyOut of 1, meaning all inputs will be joined on subjectId and subjectId appears as one column in the output, which the output is also sorted on.

One particularly annoying problem that EFJoin solved is that when the original Merge Join component was used in Full Outer Join mode, the columns that input dataflows were joined on were not merged into one column for which the output value is never null. In the output there would still exist two columns with null values where the corresponding input rows did not exist. Merging the columns together again had to be done afterwards and doing so would disrupt the sorting so that a sort component also had to be added. EFJoin prevents the need to add two extra components and an inefficient re-sorting operation every time a Full Outer Join is used.

![alt text](https://i.imgur.com/gSbEQht.png)

In the situation above where we want to do a Full Outer Join on the subjectId column of four inputs, a single EFJoin component can replace all 9 of the shown components. It will also not need to sort the dataflows three times and hence have much better performance.

# EFAggregate

EFAggregate is a component that improves on the original Aggregate component by providing more options for aggregating data from multiple rows in a dataflow. The original Aggregate component was limited to Count, Count Distinct, Minimum and Maximum, which quickly proved too restrictive for our purposes.

![alt text](https://i.imgur.com/3czFK76.png)

The Action Types (or aggregators) in the menu shown above are programmed in an adaptable, modular way that allows developers to add new aggregators with hardly any effort. Adding a simple class that inherits from Aggregator is all it takes to expand the functionality. When the class is put in the right location, the C# solution registers its existence automatically using namespace reflection.

![alt text](https://i.imgur.com/bRhvv3V.png)

The Aggregator class shown above defines a list of types it can operate on, a description given to the output column and a mapping from a list of input values to a single output value. Of these only the last one is strictly required.

The Pivot aggregator is especially interesting because it emulates SSIS’ Pivot component. The original Pivot component can only pivot one input column at a time, while EFAggregate can pivot multiple input columns at once by using the Pivot aggregator on these columns. This prevents the user from needing to split a dataflow using Multicast, pivot each of the columns one by one and join them together again using Merge Join. Using EFAggregate in such a situation is much simpler and more hassle-free.

![alt text](https://i.imgur.com/NE5fP5t.png)

All 7 of the components above can be replaced with a single EFAggregate component using Pivot aggregators.

# EFMulticast

The EFMulticast component duplicates a dataflow the way Multicast does, but gives the user the option to rename and/or exclude columns in each individual output. Also, each output itself can be given a name. This makes the component much more suitable than Multicast to organize dataflows and categorize the processes they represent.

![alt text](https://i.imgur.com/d6qKGVZ.png)

![alt text](https://i.imgur.com/d0J6ulY.png)

In the example above, each process at the outputs of EFMulticast uses only a small number of the columns provided in the input. If the regular Multicast component were used, each of these dataflows would have a large number of columns that are not used by any components downstream. In some situations, this means the flow has upwards of 20 columns while only about 3 are used. EFMulticast greatly improves the managability of the dataflows by reducing the number of columns.

# EFCompare

The EFCompare component compares column values from two different inputs. A column, identified by its user-given output sort key, is used to find corresponding rows between the two inputs similarly to how EFJoin joins rows from different inputs.

![alt text](https://i.imgur.com/4E4qK1a.png)

In the output rows, for each pair of compared columns a comparison of their content is shown. If the content is the same, this is simply an == value. If two numbers are compared and differ, it is shown which of the two is greater using < and > symbols. If two texts are compared, a div algorithm generates a comparison of the two texts which shows the location and the extent of the differences.

All previous components which emulate existing SSIS dataflow components were tested using EFCompare to be sure that their output matches the output of the original components.
