﻿using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFMulticast
{
	[DtsPipelineComponent(DisplayName = "EFMulticast",
		Description = "Duplicates rows from the input dataflow and directs them into multiple output dataflows. Enables selection and renaming of columns for each output dataflow by the user.",
		UITypeName = "EFMulticast.EFMulticastComponentUI,EFMulticast,Version=1.0.0.0,Culture=neutral,PublicKeyToken=74e1fb7e55d4e84c",
		ComponentType = ComponentType.Transform,
		IconResource = "EFMulticast.iconLarge.ico")]
	public class EFMulticast : PipelineComponent
	{
		private int[] inputBufferColumnIndexes;
		private int[][] outputBufferColumnIndexes;

		private Dictionary<int, PipelineBuffer> outputBuffersByOutputID;

		private const string inputColumnsValidProperty = "areInputColumnsValid";

		private Dictionary<string, ColumnParameters> StoredColumnParameters = new Dictionary<string, ColumnParameters>();

		#region Design Time Methods

		//Design time - constructor
		public override void ProvideComponentProperties()
		{
			// Set component information
			ComponentMetaData.Name = "EFMulticast";
			ComponentMetaData.Description = "Duplicates rows from the input dataflow and directs them into multiple output dataflows. Enables selection and renaming of columns for each output dataflow by the user.";
			ComponentMetaData.ContactInfo = "N.C. Rademaker";

			base.RemoveAllInputsOutputsAndCustomProperties();

			IDTSInput100 input = ComponentMetaData.InputCollection.New();
			input.Name = "Input 0";

			IDTSOutput100 output = ComponentMetaData.OutputCollection.New();
			output.Name = "Output 0";
			output.SynchronousInputID = input.ID;
			output.DeleteOutputOnPathDetached = true;

		}

		//Design time - Metadata Validator
		public override DTSValidationStatus Validate()
		{
			if (ComponentMetaData.InputCollection.Count != 1)
			{
				ComponentMetaData.FireError(0, ComponentMetaData.Name, "Incorrect number of inputs.", "", 0, out bool pbCancel);
				return DTSValidationStatus.VS_ISCORRUPT;
			}

			var castOutputs = ComponentMetaData.OutputCollection.Cast<IDTSOutput100>();

			while (castOutputs.Count(o => !o.IsAttached) > 1)
			{
				var firstAttached = castOutputs.First(o => !o.IsAttached);

				ComponentMetaData.OutputCollection.RemoveObjectByID(firstAttached.ID);
			}

			var outputColumns = ComponentMetaData.OutputCollection[0].OutputColumnCollection;

			var validOutputColumnIdsByOutputID = new Dictionary<int, List<int>>();

			var input = ComponentMetaData.InputCollection[0];

			var virtualInput = input.GetVirtualInput();

			var areInputColumnsValid = input.CustomPropertyCollection.GetCustomPropertyOrCreate(inputColumnsValidProperty, true);

			foreach (IDTSInputColumn100 inputColumn in input.InputColumnCollection)
			{
				IDTSVirtualInputColumn100 vColumn;
				try
				{
					vColumn = virtualInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(inputColumn.LineageID);
				}
				catch
				{
					ComponentMetaData.FireError(0, ComponentMetaData.Name, "The input column " + inputColumn.IdentificationString + " does not match a column in the upstream component.", "", 0, out bool cancel);
					areInputColumnsValid.Value = false;
					return DTSValidationStatus.VS_NEEDSNEWMETADATA;
				}

				foreach (IDTSOutput100 output in ComponentMetaData.OutputCollection)
				{
					if (!output.IsAttached || output.SynchronousInputID != 0)
					{
						continue;
					}

					var property = output.CustomPropertyCollection.GetCustomPropertyOrCreate(inputColumn.Name);

					ColumnParameters columnParameters = null;
					if (property.Value == null)
					{
						columnParameters = new ColumnParameters
						{
							Included = true,
							OutputAlias = inputColumn.Name
						};

						property.Value = columnParameters.Serialized();
					}
					else
					{
						columnParameters = ColumnParameters.DeSerialized((string[])property.Value);
					}

					var outputColumn = output.OutputColumnCollection.Cast<IDTSOutputColumn100>().FirstOrDefault(c => c.MappedColumnID == inputColumn.ID);
					if (columnParameters.Included)
					{
						if (outputColumn == null)
						{
							outputColumn = output.OutputColumnCollection.New();
						}
						outputColumn.Name = columnParameters.OutputAlias;
						outputColumn.SetDataTypeProperties(inputColumn.DataType, inputColumn.Length, inputColumn.Precision, inputColumn.Scale, inputColumn.CodePage);
						outputColumn.MappedColumnID = inputColumn.ID;
						outputColumn.SortKeyPosition = inputColumn.SortKeyPosition;

						var validOutputColumnIds = validOutputColumnIdsByOutputID.GetValueOrAddedNew(output.ID);
						validOutputColumnIds.Add(outputColumn.ID);
					}
				}
			}

			foreach (IDTSOutput100 output in ComponentMetaData.OutputCollection)
			{
				if (output.IsAttached)
				{
					var isSorted = false;

					var invalidOutputColumnIds = new List<int>();
					foreach (IDTSOutputColumn100 outputColumn in output.OutputColumnCollection)
					{
						if (!validOutputColumnIdsByOutputID.ContainsKey(output.ID) || !validOutputColumnIdsByOutputID[output.ID].Contains(outputColumn.ID))
						{
							invalidOutputColumnIds.Add(outputColumn.ID);
						}
						else
						{
							if (outputColumn.SortKeyPosition != 0)
							{
								isSorted = true;
							}
						}
					}

					foreach (var invalidId in invalidOutputColumnIds)
					{
						output.OutputColumnCollection.RemoveObjectByID(invalidId);
					}

					if (output.SynchronousInputID == 0)
					{
						output.IsSorted = isSorted;
					}
				}
			}

			return DTSValidationStatus.VS_ISVALID;
		}

		//Design Time - method to autocorrect VS_NEEDSNEWMETADATA error
		public override void ReinitializeMetaData()
		{
			foreach (IDTSInput100 input in ComponentMetaData.InputCollection)
			{
				var areInputColumnsValid = input.CustomPropertyCollection.GetCustomPropertyOrCreate(inputColumnsValidProperty, false);
				if (!(bool)areInputColumnsValid.Value)
				{
					IDTSVirtualInput100 vInput = input.GetVirtualInput();

					foreach (IDTSInputColumn100 column in input.InputColumnCollection)
					{
						IDTSVirtualInputColumn100 vColumn = vInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(column.LineageID);

						if (vColumn == null)
							input.InputColumnCollection.RemoveObjectByID(column.ID);
					}
					areInputColumnsValid.Value = true;
				}
			}
		}

		public override void OnInputPathAttached(int inputID)
		{
			var input = ComponentMetaData.InputCollection.GetObjectByID(inputID);
			var vInput = input.GetVirtualInput();

			for (int i = 0; i < vInput.VirtualInputColumnCollection.Count; i++)
			{
				var vColumn = vInput.VirtualInputColumnCollection[i];

				if (!input.InputColumnCollection.Cast<IDTSInputColumn100>().Any(ic => ic.LineageID == vColumn.LineageID))
				{
					var newColumn = input.InputColumnCollection.New();
					newColumn.LineageID = vColumn.LineageID;
					newColumn.Name = vColumn.Name;
				}
			}

			Validate();
		}

		public override void OnInputPathDetached(int inputID)
		{
			var input = ComponentMetaData.InputCollection.GetObjectByID(inputID);

			foreach (IDTSOutput100 output in ComponentMetaData.OutputCollection)
			{
				output.OutputColumnCollection.RemoveAll();
			}

			input.InputColumnCollection.RemoveAll();

			Validate();
		}

		public override void OnOutputPathAttached(int outputID)
		{
			int outputCount = 0;

			while (ComponentMetaData.OutputCollection.Cast<IDTSOutput100>().Any(o => o.Name == "Output " + outputCount))
			{
				outputCount++;
			}
			var newOutput = ComponentMetaData.OutputCollection.New();
			newOutput.Name = "Output " + outputCount;
			newOutput.SynchronousInputID = ComponentMetaData.InputCollection[0].ID;
			newOutput.DeleteOutputOnPathDetached = true;

			Validate();
		}

		//Override InsertOutputColumnAt to prevent addition of new column from Advanced Editor
		public override IDTSOutputColumn100 InsertOutputColumnAt(
			 int outputID,
			 int outputColumnIndex,
			 string name,
			 string description)
		{
			ComponentMetaData.FireError(0, ComponentMetaData.Name, "Output columns cannot be added to " + ComponentMetaData.Name, "", 0, out bool cancel);
			//bubble-up the error to VS
			throw new Exception("Output columns cannot be added to " + ComponentMetaData.Name, null);
		}

		#endregion Design Time Methods

		#region Run Time Methods

		//Run Time - Pre Execute identifying the input columns in this component from Buffer Manager
		public override void PreExecute()
		{
			var input = ComponentMetaData.InputCollection[0];

			inputBufferColumnIndexes = new int[input.InputColumnCollection.Count];
			for (int i = 0; i < input.InputColumnCollection.Count; i++)
			{
				inputBufferColumnIndexes[i] = BufferManager.FindColumnByLineageID(input.Buffer, input.InputColumnCollection[i].LineageID);
			}

			outputBufferColumnIndexes = new int[ComponentMetaData.OutputCollection.Count - 1][];
			for (int i = 0; i < ComponentMetaData.OutputCollection.Count - 1; i++)
			{
				var output = ComponentMetaData.OutputCollection[i];

				outputBufferColumnIndexes[i] = new int[output.OutputColumnCollection.Count];
				for (int j = 0; j < output.OutputColumnCollection.Count; j++)
				{
					outputBufferColumnIndexes[i][j] = BufferManager.FindColumnByLineageID(output.Buffer, output.OutputColumnCollection[j].LineageID);
				}
			}
		}

		public override void PrimeOutput(int outputs, int[] outputIDs, PipelineBuffer[] buffers)
		{
			outputBuffersByOutputID = new Dictionary<int, PipelineBuffer>();
			for (int i = 0; i < buffers.Length; i++)
			{
				outputBuffersByOutputID[outputIDs[i]] = buffers[i];
			}
		}

		public override void ProcessInput(int inputID, PipelineBuffer buffer)
		{
			if (!buffer.EndOfRowset)
			{
				var input = ComponentMetaData.InputCollection.GetObjectByID(inputID);

				while (buffer.NextRow())
				{
					for (int i = 0; i < ComponentMetaData.OutputCollection.Count; i++)
					{
						var output = ComponentMetaData.OutputCollection[i];

						if (!output.IsAttached || output.SynchronousInputID != 0)
						{
							continue;
						}

						var outputBuffer = outputBuffersByOutputID[output.ID];

						outputBuffer.AddRow();

						for (int j = 0; j < input.InputColumnCollection.Count; j++)
						{
							var inputColumn = input.InputColumnCollection[j];

							var columnParameters = StoredColumnParameters[inputColumn.Name];

							if (columnParameters.Included)
							{
								for (int k = 0; k < output.OutputColumnCollection.Count; k++)
								{
									if (output.OutputColumnCollection[k].Name == columnParameters.OutputAlias)
									{
										var value = buffer[inputBufferColumnIndexes[j]];

										var blobValue = value as BlobColumn;
										if (blobValue != null)
										{
											if (blobValue.Length > 0)
											{
												value = blobValue.GetBlobData(0, Convert.ToInt32(blobValue.Length));
											}
											else
											{
												value = null;
											}
										}

										if (value != null)
										{
											outputBuffer[outputBufferColumnIndexes[i][k]] = value;
										}
										else
										{
											outputBuffer.SetNull(outputBufferColumnIndexes[i][k]);
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				if (outputBuffersByOutputID != null)
				{
					foreach (var outputBuffer in outputBuffersByOutputID)
					{
						outputBuffer.Value.SetEndOfRowset();
					}
				}
			}
		}

		#endregion Run Time Methods
	}
}
