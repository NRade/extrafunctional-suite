﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data;

using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;

namespace EFMulticast
{
    public partial class EFMulticastUITabPage : TabPage
    {
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn Datatype;
        private DataGridViewTextBoxColumn InputColumn;
        private DataGridViewCheckBoxColumn Include;
        private DataGridViewTextBoxColumn OutputAlias;
        private CheckBox synchronizedCheckBox;
        private TextBox textBox1;
        private TextBox outputNameTextBox;
        private Label outputNameLabel;
        private bool freezeCellValueChangedEvent = true;

        private int outputIndex;
        private IDTSComponentMetaData100 metaData;
        private IDTSOutput100 output;

        public EFMulticastUITabPage(IDTSComponentMetaData100 metaData, int outputIndex)
        {
            InitializeComponent();

            this.outputIndex = outputIndex;
            this.metaData = metaData;
            output = metaData.OutputCollection[outputIndex];

            outputNameTextBox.Text = output.Name;

            var synchronous = output.SynchronousInputID == metaData.InputCollection[0].ID;
            synchronizedCheckBox.Checked = synchronous;

            if (!synchronous)
            {
                InitializeDataGridRows();
            }

            dataGridView1.CellMouseUp += DataGridView1_OnCellMouseUp;

            dataGridView1.CellEndEdit += DataGridView1_CellEndEdit;

            dataGridView1.CellValueChanged += DataGridView1_CellValueChanged;

            synchronizedCheckBox.CheckedChanged += SynchronizedCheckBox_CheckedChanged;

            freezeCellValueChangedEvent = false;

            RefreshInfo();
        }

        private void InitializeDataGridRows()
        {
            dataGridView1.SuspendLayout();

            foreach (IDTSInput100 input in metaData.InputCollection)
            {
                var vInput = input.GetVirtualInput();

                foreach (IDTSVirtualInputColumn100 vInputColumn in vInput.VirtualInputColumnCollection)
                {
                    var property = output.CustomPropertyCollection.GetCustomPropertyOrCreate(vInputColumn.Name);

                    ColumnParameters columnParameters;
                    if (property.Value == null)
                    {
                        columnParameters = new ColumnParameters
                        {
                            Included = true,
                            OutputAlias = vInputColumn.Name
                        };
                        property.Value = columnParameters.Serialized();
                    } else
                    {
                        columnParameters = ColumnParameters.DeSerialized((string[])property.Value);
                    }
                    
                    var lastIndex = dataGridView1.Rows.Add(new object[] {
                        Convert.ToString(vInputColumn.DataType),
                        vInputColumn.Name,
                        columnParameters.Included,
                        columnParameters.OutputAlias
                    });
                }
            }

            dataGridView1.ResumeLayout(true);
        }

        private void SynchronizedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(((CheckBox)sender).Checked)
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Enabled = false;
            } else
            {
                InitializeDataGridRows();
                dataGridView1.Enabled = true;
            }
        }

        public void HandleConfirmation()
        {
            output.Name = outputNameTextBox.Text;

            output.SynchronousInputID = synchronizedCheckBox.Checked ? metaData.InputCollection[0].ID : 0;

            if (!synchronizedCheckBox.Checked)
            {
                var columnParameters = GetColumnParameters();

                for (int j = 0; j < metaData.InputCollection[0].InputColumnCollection.Count; j++)
                {
                    var inputColumn = metaData.InputCollection[0].InputColumnCollection[j];

                    var property = output.CustomPropertyCollection.GetCustomPropertyOrCreate(inputColumn.Name);

                    property.Value = columnParameters[j].Serialized();
                }
            }
        }

        public ColumnParameters[] GetColumnParameters()
        {
            var columnParameterList = new ColumnParameters[metaData.InputCollection[0].GetVirtualInput().VirtualInputColumnCollection.Count];

            for(int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                columnParameterList[i] = new ColumnParameters
                {
                    Included = (bool)dataGridView1.Rows[i].Cells[2].Value,
                    OutputAlias = (string)dataGridView1.Rows[i].Cells[3].Value
                };
            }

            return columnParameterList;
        }
        
        private void DataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!freezeCellValueChangedEvent && e.ColumnIndex == 2 && e.RowIndex != -1)
            {
                RefreshInfo();
            }
        }

        private void DataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            RefreshInfo();
        }

        private void DataGridView1_OnCellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex != -1)
            {
                dataGridView1.EndEdit();
            }
        }

        private List<string> validationMessages = new List<string>();

        private void RefreshInfo()
        {

        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Datatype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InputColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Include = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.OutputAlias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.outputNameTextBox = new System.Windows.Forms.TextBox();
            this.outputNameLabel = new System.Windows.Forms.Label();
            this.synchronizedCheckBox = new CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Datatype,
            this.InputColumn,
            this.Include,
            this.OutputAlias});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = ScrollBars.Vertical;
            this.dataGridView1.AutoSize = true;
            this.dataGridView1.Location = new System.Drawing.Point(0, 30);
            // 
            // Datatype
            // 
            this.Datatype.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Datatype.HeaderText = "Datatype";
            this.Datatype.Name = "Datatype";
            this.Datatype.ReadOnly = true;
            this.Datatype.Width = 75;
            // 
            // InputColumn
            // 
            this.InputColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.InputColumn.HeaderText = "Input Column";
            this.InputColumn.Name = "InputColumn";
            this.InputColumn.ReadOnly = true;
            this.InputColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.InputColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.InputColumn.Width = 75;
            // 
            // Include
            // 
            this.Include.HeaderText = "Include";
            this.Include.Name = "Include";
            this.Include.Width = 48;
            // 
            // OutputAlias
            // 
            this.OutputAlias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OutputAlias.HeaderText = "Output Alias";
            this.OutputAlias.Name = "OutputAlias";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(12, 533);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(653, 20);
            // 
            // outputNameTextBox
            // 
            this.outputNameTextBox.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
            this.outputNameTextBox.Name = "outputNameTextBox";
            
            // 
            // outputNameLabel
            // 
            this.outputNameLabel.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            this.outputNameLabel.Name = "outputNameLabel";
            this.outputNameLabel.Text = "Name:";
            // 
            // synchronizedCheckBox
            // 
            this.synchronizedCheckBox.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            this.synchronizedCheckBox.Text = "Fully synchronized (pass on all columns unchanged)";
            
            // 
            // EFMulticastUIForm
            // 
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "EFMulticastUIPanel";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

            this.Size = new System.Drawing.Size(600, 600);

            this.outputNameTextBox.Location = new System.Drawing.Point(310, 5);
            this.outputNameTextBox.Size = new System.Drawing.Size(290, 20);

            this.outputNameLabel.Location = new System.Drawing.Point(270, 7);
            this.outputNameLabel.Size = new System.Drawing.Size(40, 20);

            this.synchronizedCheckBox.Location = new System.Drawing.Point(0, 0);
            this.synchronizedCheckBox.Size = new System.Drawing.Size(270, 30);

            this.Controls.Add(this.synchronizedCheckBox);
            this.Controls.Add(this.outputNameTextBox);
            this.Controls.Add(this.outputNameLabel);
        }
    }
}
