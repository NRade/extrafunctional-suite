﻿using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFMulticast
{
    public static class DTSExtensions
    {
        private static Dictionary<DataType, Type> DTSTypeToDNType = new Dictionary<DataType, Type>();

        private static Dictionary<Type, List<DataType>> DNTypeToDTSTypes = new Dictionary<Type, List<DataType>>();

        public static Type GetDNType(DataType dataType)
        {
            return DTSTypeToDNType[dataType];
        }

        public static List<DataType> GetDTSTypes(Type type)
        {
            return DNTypeToDTSTypes[type];
        }

        static DTSExtensions()
        {
            AddTypeMapping(DataType.DT_BOOL, typeof(bool));

            AddTypeMapping(DataType.DT_BYTES, typeof(byte[]));

            AddTypeMapping(DataType.DT_DBTIMESTAMPOFFSET, typeof(DateTimeOffset));

            AddTypeMapping(DataType.DT_DECIMAL, typeof(decimal));

            AddTypeMapping(DataType.DT_FILETIME, typeof(DateTime));
            AddTypeMapping(DataType.DT_DATE, typeof(DateTime));
            AddTypeMapping(DataType.DT_DBDATE, typeof(DateTime));
            AddTypeMapping(DataType.DT_DBTIME, typeof(DateTime));
            AddTypeMapping(DataType.DT_DBTIME2, typeof(DateTime));
            AddTypeMapping(DataType.DT_DBTIMESTAMP, typeof(DateTime));
            AddTypeMapping(DataType.DT_DBTIMESTAMP2, typeof(DateTime));

            AddTypeMapping(DataType.DT_GUID, typeof(Guid));

            AddTypeMapping(DataType.DT_I1, typeof(byte));
            AddTypeMapping(DataType.DT_UI1, typeof(byte));

            AddTypeMapping(DataType.DT_I2, typeof(short));
            AddTypeMapping(DataType.DT_UI2, typeof(short));

            AddTypeMapping(DataType.DT_I4, typeof(int));
            AddTypeMapping(DataType.DT_UI4, typeof(int));

            AddTypeMapping(DataType.DT_I8, typeof(long));
            AddTypeMapping(DataType.DT_UI8, typeof(long));

            AddTypeMapping(DataType.DT_R4, typeof(double));
            AddTypeMapping(DataType.DT_R8, typeof(double));
            AddTypeMapping(DataType.DT_NUMERIC, typeof(double));
            AddTypeMapping(DataType.DT_CY, typeof(double));

            AddTypeMapping(DataType.DT_WSTR, typeof(string));
            AddTypeMapping(DataType.DT_STR, typeof(string));
            AddTypeMapping(DataType.DT_TEXT, typeof(string));
            AddTypeMapping(DataType.DT_NTEXT, typeof(string));
        }

        public static void AddTypeMapping(DataType dataType, Type type)
        {
            DTSTypeToDNType.Add(dataType, type);

            var dataTypesList = DNTypeToDTSTypes.GetValueOrAddedNew(type);
            dataTypesList.Add(dataType);
        }

        public static DataType GetDataTypeFromString(string text)
        {
            return Enum.GetValues(typeof(DataType)).Cast<DataType>().FirstOrDefault(dt => dt.ToString() == text);
        }

        public static T1 GetCustomPropertyValue<T1>(this IDTSComponentMetaData100 metaData, string name)
        {
            return (T1)metaData.GetCustomPropertyOrCreate(name).Value;
        }

        public static T1 GetCustomPropertyValue<T1>(this IDTSCustomPropertyCollection100 customProperties, string name)
        {
            return (T1)customProperties.GetCustomPropertyOrCreate(name).Value;
        }

        public static Type DNTypeFromDTSString(string dtsTypeString)
        {
            return DTSTypeToDNType[GetDataTypeFromString(dtsTypeString)];
        }

        public static IDTSCustomProperty100 GetCustomPropertyOrCreate(this IDTSComponentMetaData100 metaData, string name, object defaultValue = null)
        {
            return metaData.CustomPropertyCollection.GetCustomPropertyOrCreate(name, defaultValue);
        }

        public static IDTSCustomProperty100 GetCustomPropertyOrCreate(this IDTSCustomPropertyCollection100 customProperties, string name, object defaultValue = null)
        {
            var customProperty = customProperties.Cast<IDTSCustomProperty100>().FirstOrDefault(p => p.Name == name);

            if (customProperty == null)
            {
                customProperty = customProperties.New();
                customProperty.Name = name;
                customProperty.Value = defaultValue;
            }

            return customProperty;
        }
    }
}
